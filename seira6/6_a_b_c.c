#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdint.h>
#define _NOP() do { __asm__ __volatile__ ("nop"); } while (0)
#include <string.h>

#define Ex6a
#define Ex6b
#define Ex6c
#define Ex6d

void lcd_data(char x);

void USART_init(void)
{   
    UCSRA = 0;
    UCSRB = (1<<RXEN) | (1<<TXEN);
    UBRRH = 0;
    UBRRL = 51;
    UCSRC = (1 << URSEL) | (3 << UCSZ0);
}


void USART_transmit(char input){
								// check if usart is ready to transmit.
	while((UCSRA & 0x20) == 0);			// if no check again
	UDR = input;						// else transmit the input byte to usart
	
}

void USART_putstring(char* StringPtr){			// this function is used to print 
								// a string to usart
	while(*StringPtr != '\0')				// if you haven't reached the end of string
		USART_transmit(*StringPtr++);			// transit the current character of the string
}

unsigned char USART_receive(void)
{   
    while((UCSRA & 0x80) == 0 ){}
    return UDR; 
}

char data[32];
void USART_string_receive(void)
{
    int i = 0;
    char c;
    
    while((c = USART_receive()) != '\n')
        data[i++] = c;
    data[i] = 0;
}

void lcd_string_data(char * string)
{
    while(*string)
        lcd_data(*string++);
}

//************************************************************************************
//************************************************************************************

void init_keyboard()
{
    DDRC = 0xf0;
}

int msb(int x)
{
    int res = 0;
    while(x > 1){
        res++;
        x>>=1;
    }
    return res;
}

//only one key pressed at a time
//if nothing pressed: return -1
int scan_keypad()
{
    int key;
    for(int i = 4;i < 8;i++){
        PORTC = 1<<i;
        _NOP();
		_NOP();
        key = PINC & 0xf;
        if(key != 0)
            return 4*(i-4)+msb(key);
    }
    return -1;
}
int prev = -1;

int read_spark(){
	int t1,t2;
	do{
		t1 = scan_keypad();
		_delay_ms (16);
		t2 = scan_keypad();
	}while(t1 != t2);
	return t2;
}

int scan_rising_edge()
{
    do{
        int tmp;
        while((tmp = read_spark()) == prev)
            ;
        prev = tmp;
    } while(prev == -1);
	return prev;
}

void pd3_pulse()
{
    char temp = PORTD;
    PORTD = temp | (1<<3);
    PORTD = temp;
}

void write_2_nibbles(char x)
{
    char lsb4 = PORTD & 0x0f;
    PORTD = lsb4 | (x & 0xf0);
    pd3_pulse();
    PORTD = lsb4 | (x << 4);
    pd3_pulse();
}

void lcd_data(char x)
{
    PORTD = PORTD | (1<<2);
    write_2_nibbles(x);
    _delay_us(43);
}


void lcd_command(char x)
{
    PORTD = PORTD & (~(1<<2));
    write_2_nibbles(x);
    _delay_us(39);
}


void lcd_init()
{
    DDRD = 0xff;

    _delay_ms(40);
    PORTD = 0x30;
    pd3_pulse();
    _delay_us(39);
    
    PORTD = 0x30;
    pd3_pulse();
    _delay_us(39);
    
    PORTD = 0x20;
    pd3_pulse();
    _delay_us(39);
    
    lcd_command(0x28);
    lcd_command(0x0c);
    lcd_command(0x01);
    
    _delay_us(1530);
    lcd_command(0x06);
    
}

int comp2_to_celcius(uint8_t msbs,uint8_t lsbs)
{
   int16_t celcius = (msbs << 8) + lsbs;
   return celcius / 2;
    
}

int one_wire_reset()
{
	DDRA |= (1 << 4);
	PORTA &= ~(1<<4);
	_delay_us(480);
	DDRA &= ~(1 << 4);
	PORTA &= ~(1<<4);
	_delay_us(100);
	char PA4_digit = PINA & 0x10;  //care only about PA4
	_delay_us(380);
	if(PA4_digit) return 0;
	else return 1;
}

int one_wire_receive_bit()
{
	DDRA |= (1 << 4);
	PORTA &= ~(1<<4);
	_delay_us(2);
	DDRA &= ~(1 << 4);
	PORTA &= ~(1<<4);
	_delay_us(10);
	char bit = (PINA >> 4) & 1;
 
	_delay_us(49);
	return bit;
 }

int one_wire_receive_byte()
 {
	unsigned int res = 0;
	for(int i = 0;i < 8;i++)
		res = (res >> 1) | (one_wire_receive_bit() << 7);
	return res;
}
 
 void one_wire_transmit_bit(char x)
 {
	DDRA |= (1 << 4);
	PORTA &= ~(1<<4);
	_delay_us(2);
	x = x & 1;
	if(x) PORTA |= (1<<4);
	else PORTA &= ~(1<<4);
	_delay_us(58);
	DDRA &= ~(1 << DDA4);
	PORTA &= ~(1<<4);
	_delay_us(1);
}
 
void one_wire_transmit_byte(char x)
{
	for(int i=0;i<8;i++){
		one_wire_transmit_bit(x & 1);
		x >>= 1;
	}
}


int read_temperature()
{

	int temperature = 0x8000;
	
	int detect = one_wire_reset();
	if(!detect) return 0x8000;
	one_wire_transmit_byte(0xCC);
	one_wire_transmit_byte(0x44);
	// wait to complete
	while(one_wire_receive_bit());
	//lcd_data('1');
	detect = one_wire_reset();
	if(detect){
		one_wire_transmit_byte(0xCC);
		one_wire_transmit_byte(0xBE);
		uint8_t lsbits = one_wire_receive_byte();  //if it reads 8-MSB first
		uint8_t msbits = one_wire_receive_byte();
		temperature = comp2_to_celcius(msbits,lsbits);
	}
	return temperature;
	
}

//**********************************************************************************

int read_and_disp(int num)
{
    USART_string_receive();
	//lcd_string_data(data);

	//NEW LINE remove
    if (strcmp(data,"\"Success\"")==0){
		lcd_init();
        lcd_data('0'+num);
        lcd_string_data(".Success");
		return 1;
    }
    else if (strcmp(data,"\"Fail\"")==0){
        lcd_init();
		lcd_data('0'+num);
        lcd_string_data(".Fail");
    	return 0;
	}
}

int main (void)
{
   USART_init();
   lcd_init();
    
    char str[128];
#ifdef Ex6a
    //1h entolh teamname
    
    USART_putstring("teamname: \"A7\"\n");

    read_and_disp(1);
    
    _delay_ms(2000);    //mikri kathisterisi gia 2h entolh
    
    //2h entolh connect
    USART_putstring("connect\n");
    
    read_and_disp(2);
#endif
while(1){
_delay_ms(2000);
#ifdef Ex6b
    //3h entolh
    int received_temperature = read_temperature() >> 3;
    
    sprintf(str,"payload: [{\"name\": \"Temperature\",\"value\": %d}]\n",received_temperature);
    USART_putstring(str);
    
    //strcpy(str,"transmit");              //isws gia metadosh tou payload sto 6_d
    //USART_string_transmit(str);
    
     read_and_disp(3);

#endif
_delay_ms(2000);
//4h entolh
#ifdef Ex6c

    char t[16] = {1,2,3,0xA,4,5,6,0xB,7,8,9,0xC,0xE,0,0xF,0xD};
    char to_ascii[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    
    init_keyboard();
    
    //int pliktro = t[scan_rising_edge()]; // TODO
    int pliktro = t[read_spark()]; // TODO
    if(pliktro==7){
		
        USART_putstring("ready: \"true\"\n");
    	//lcd_init();
       //lcd_string_data("4.Success");
    	read_and_disp(4);
	}
    else{
        USART_putstring("ready: \"false\"\n");
    	//lcd_init();
        //lcd_string_data("4.Fail");
    	read_and_disp(4);
	}

#endif
_delay_ms(2000);
#ifdef Ex6d
	USART_putstring("transmit\n");
	lcd_init();
	USART_string_receive();
	lcd_string_data(data);
#endif

}

}
