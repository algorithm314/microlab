---
mainfont: Open Sans
documentclass: extarticle
fontsize: 12pt
geometry: margin=1in
papersize: a4
title: "5η Σειρά ασκήσεων, Εργαστήριο Μικροϋπολογιστών"
author:
- "Αγιάννης Κωνσταντίνος 03116352"
- "Τσιαχρήστος Δημήτριος 03116048"
---

# Ζήτημα 5.1 

### (α) Κώδικας σε asm

Στο Message1 είναι το μήνυμα που θα μεταδώσουμε και τελειώνει στο '\0'.usart_init για αρχικοποίηση usart και usart_transmit για 
μετάδοση των byte. Με lpm r24, Z+  φορτώνει το μήνυμα bit bit και κάθε φορά πάει στον επόμενο χαρακτήρα.

```asm
.include "m16def.inc"
.org 0x00

.CSEG
rjmp main

Message1:
.db "Geia sou kosme!", '\n','\r', '\0'			; Write a string in ram
main:
	ldi ZL, low(Message1<<1)		; Initialize Z pointer in order to point to
	ldi ZH, high(Message1<<1)		; the first element of the string
						; which is basically an array of characters

set_stack:					; Initialize Stack
	ldi r24, low(RAMEND)
	out SPL, r24
	ldi r24, high(RAMEND)
	out SPH, r24

print_strint_to_uart:

     rcall usart_init			; Initialize usart
 
	 loop_:
		lpm r24, Z+			; Loads the context of where Z points to r24   and every time it goes to the next place

		cpi r24, '\0'			; If you reach the end of string go to next_ branch
		breq main

		rcall usart_transmit		; Transmit to usart the context of r24
		
		push r24			; Save the value of r24 on stack

		ldi r24,low(100)		; Wait 100 msec for the results		
		ldi r25,high(100)		; to be presented more clearly
		rcall wait_msec

		pop r24			; Restore the value of r24
		
		rjmp loop_

	; ------------------------ ;
	; ------- ROUTINES ------- ;
	; ------------------------ ;

	; Routine: usart_init 
	; Description: 
	; This routine initializes the 
	; usart as shown below. 
	; ------- INITIALIZATIONS ------- 
	; 
	; Baud rate: 9600 (Fck = 8MHz) 
	; Asynchronous mode 
	; Transmitter on 
	; Reciever on 
	; Communication parameters: 8 Data ,1 Stop , no Parity 
	; -------------------------------- 
	; parameters: None. 
	; return value: None. 
	; registers affected: r24 
	; routines called: None

	usart_init: 
		clr r24					; initialize UCSRA to zero	 
		out UCSRA ,r24 
		ldi r24 ,(1<<RXEN) | (1<<TXEN)		; activate transmitter/receiver 
		out UCSRB ,r24 
		ldi r24 ,0					; baud rate = 9600 
		out UBRRH ,r24 
		ldi r24 ,51 
		out UBRRL ,r24 
		ldi r24 ,(1 << URSEL) | (3 << UCSZ0)	; 8-bit character size, 
		out UCSRC ,r24				; 1 stop bit 
		ret 
	
	; Routine: usart_transmit 
	; Description: 
	; This routine sends a byte of data 
	; using usart. 
	; parameters: 
	; r24: the byte to be transmitted 
	; must be stored here. 
	; return value: None. 
	; registers affected: r24 
	; routines called: None.

usart_transmit: 
	sbis UCSRA ,UDRE					; check if usart is ready to transmit 
	rjmp usart_transmit					; if no check again, else transmit 
	out UDR ,r24						; content of r24 
	ret 
	
	; Routine: usart_receive 
	; Description: 
	; This routine receives a byte of data 
	; from usart. 
	; parameters: None. 
	; return value: the received byte is 
	; returned in r24. 
	; registers affected: r24 
	; routines called: None. 
	
wait_usec:
	sbiw r24 ,1		; 2 cycles (0.250 micro sec)
	nop			; 1 cycles (0.125 micro sec)
	nop			; 1 cycles (0.125 micro sec)
	nop			; 1 cycles (0.125 micro sec)
	nop			; 1 cycles (0.125 micro sec)
	brne wait_usec	; 1 or 2 cycles (0.125 or 0.250 micro sec)
	ret			; 4 cycles (0.500 micro sec)

wait_msec:
	push r24		; 2 cycles (0.250 micro sec)
	push r25		; 2 cycles
	ldi r24 , 0xe6	; load register r25:r24 with 998 (1 cycles - 0.125 micro sec)
	ldi r25 , 0x03	; 1 cycles (0.125 micro sec)
	rcall wait_usec	; 3 cycles (0.375 micro sec), total delay 998.375 micro sec
	pop r25		; 2 cycles (0.250 micro sec)
	pop r24		; 2 cycles
	sbiw r24 , 1		; 2 cycles
	brne wait_msec	; 1 or 2 cycles (0.125 or 0.250 micro sec)
	ret
```


### (β) Κώδικας σε c

Οι συναρτήσεις επικοινωνίας uart και adc είναι αντίστοιχες με τις δοσμένες υπορουτίνες σε assembly. Υπάρχει μία επιπλέον 
η USART_putstring για εκτύπωση του string στην usart. Αρχικά διαβάζεται ο αριθμός από την uart. Μετά ελέγχεται αν είναι 
κατάλληλος και τυπώνονται ανάλογα μηνύματα.

```c
#include <avr/io.h> 
#include <stdio.h>

//Declaration of functions
void USART_init(void);					
unsigned char USART_receive(void);		
void USART_transmit(char);				
void USART_putstring(char*);			

int main(void){  
	
	//Declaration of variables
	char check,output;
	char msg1[] = "Invalid Number\n\r" ;
	char msg2[] = "Read " ;
	char msg3[] = "\n\r" ;
	
	DDRB = 0xFF;						// set PORTΒ as output
	
	USART_init();						// initialize usart
	
	while (1){
		
		check = USART_receive();	    //receive byte from usart
		if ( (check < '0') || (check > '8') ){  	// if the byte is not a number	 between 0 and 8
			USART_putstring(msg1);		// print InvalidNumber, change line
			continue;				// and go through while loop from the top
		}
		//char c;
		//while((c = USART_receive()) != \n);
		USART_putstring(msg2);			// else print ReadX , 
		USART_transmit(check);			// which X is the number we received
		USART_putstring(msg3);			// and change line
		
		output = check - '0';			// transform ascii to value(HEX)
		PORTB = (1<<output)-1;				// light the led suited to input number

	}

	return 0;                  
}
void USART_init(void){
	
	UCSRA = 0;						// initialize UCSRA to zero
	UCSRB = (1<<RXEN) | (1<<TXEN);			// activate transmitter/receiver 
	UBRRH = 0;						// baud rate = 9600 
	UBRRL = 51;
	UCSRC = (1 << URSEL) | (3 << UCSZ0);		// 8-bit character size, 1 stop bit

}

unsigned char USART_receive(void){
								// check if usart received a byte.
	while((UCSRA & 0x80) == 0);	//		// if no check again              //RXC is the bit7 of UCSRA
	return UDR;						// else return the received byte 
	
}

void USART_transmit(char input){
								// check if usart is ready to transmit.
	while((UCSRA & 0x20) == 0);			// if no check again                      //UDRE is the bit5 of UCSRA
	UDR = input;						// else transmit the input byte to usart
	
}

void USART_putstring(char* StringPtr){			// this function is used to print 
								               // a string to usart
	while(*StringPtr != '\0')				// if you haven't reached the end of string
		USART_transmit(*StringPtr++);			// transmit the current character of the string
}
#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <stdint.h>

#define USART_BAUDRATE 9600
#define BAUD_PRESCALE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)

void USART_init(void)
{   
    UCSRA = 0;
    UCSRB = (1<<RXEN) | (1<<TXEN);
    UBRRH = 0;
    UBRRL = 51;
    UCSRC = (1 << URSEL) | (3 << UCSZ0);
}

unsigned char USART_receive(void)
{   
    while((UCSRA & 0x80) == 0 ){}        
    return UDR; 
}

void USART_transmit(char input)
{
    while((UCSRA & 0x20) == 0){}         
    UDR = input;
}

void ADC_init ()
{
    ADMUX = (1 << REFS0);
    ADCSRA = (1 << ADEN) | (1 << ADPS0) | (1 << ADPS1) | (1 << ADPS2);
}

int adc_read()
{
	ADCSRA = ADCSRA | 0x40;       //conversion starts when ADSC bit(bit 6 of ADCSRA) becomes 1

	while ( !(ADCSRA & 0x40) ) { /* WAIT UNTIL CONVERSION IS DONE */ }
        
	return ADC;
}

/*
int main (void)
{
    
     //ADCSRA [ADEN|ADSC|ADATE|ADIF|ADIE|*|*|*]
    
    
    // Left Adjust Result: No
    // ADLAR = 0;
    
    // Initialize USART
    USART_init();

    // Initialize ADC
    ADC_init();


    while (1) {
	    // To start ADC conversion ADSC=1
        uint32_t res = (uint32_t)adc_read();    //read from UART
		res = res *500/1024;                    //voltage conversion given that Vcc=5
        USART_transmit ( res/100 + '0' );       //msdigit before .
        USART_transmit('.');
		USART_transmit((res/10)%10+'0');   //first digit after .
		USART_transmit(res%10+'0');        //second digit after .
        USART_transmit('\n');              //new line
		USART_transmit('\r');
        
        _delay_ms(200);    //delay 200ms
         
	}
}
*/
```


# Ζήτημα 5.2 

### (α) Κώδικας σε asm

Για τη μετατροπή τάσης βάσει του τύπου Vin=(ADC/1024)*5 κατασκευάζονται οι τελευταίοι 3 πίνακες με αντιγραφή από το terminal σε python.
Λόγω εξοικονόμησης χώρου χρησιμοποιούνται 8 bits αντί για 10  που είναι αποδεκτό καθώς για το 500 χρειάζονται το
πολύ 9 bits και λαμβάνοντας υπόψη την ακρίβεια της adc το 9ο bit θα μπορούσε να παραλειφεί. 0x1C  είναι η διεύθυνση διακοπής.
Στον r19 υπολογίζεται κάθε φορά ο ζητούμενος μετρητής κάθε 200ms και εμφανίζεται στην PORTB.

```asm
.include "m16def.inc"

.org 0
jmp main

.org 0x1c        //address of interrupt
jmp routine


main:
	ldi r24, low(RAMEND)        //initialize stack
	out SPL, r24
	ldi r24, high(RAMEND)
	out SPH, r24
	ldi r19,0                  //initialize counter
	rcall usart_init           //initialize usart
	rcall ADC_init              //initialize adc
	ser r24
	out DDRB,r24             //PORTB as output  for the counter

routine:
	in r20,ADCL          //first read ADCL register
	in r21,ADCH          //then ADCH 


	lsr r21              
	ror r20
	lsr r21
	ror r20
	
	out PORTB,r19             //counter in PORTB
	inc r19                  //increase counter
	ldi zl,LOW(a1*2)         //a1 matrix is at the end  for the ms_digit before .
	ldi zh,HIGH(a1*2)
	add zl,r20                    //add first to ADCL
	adc zh,r21                    //then with carry to ADCH
	lpm r24,Z                      //load z to r24 for transmit
	rcall usart_transmit          //send the byte using usart
	
	ldi r24,'.'
	rcall usart_transmit
	
	ldi zl,LOW(a2*2)           //a2 matrix is at the end  for the first digit after .
	ldi zh,HIGH(a1*2)
	add zl,r20               
	adc zh,r21               
	lpm r24,Z                
	rcall usart_transmit

	ldi zl,LOW(a3*2)         //a3 matrix is at the end  for the second digit after .
	ldi zh,HIGH(a3*2)
	add zl,r20              
	adc zh,r21              
	lpm r24,Z                
	rcall usart_transmit
	
	ldi r24,'\n'               //new line
	rcall usart_transmit
	ldi r24,'\r'
	rcall usart_transmit
	ldi r24,LOW(200)        //increase counter every 200ms
	ldi r25,HIGH(200)
	rcall wait_msec
	sbi ADCSRA,ADSC         //set ADSC bit of ADCSRA for conversion start
	reti                   //return from interrupt
	
ADC_init:
	ldi r24,(1<<REFS0) ; Vref: Vcc
	out ADMUX,r24
	;MUX4:0 = 00000 for A0.
	;ADC is Enabled (ADEN=1)
	;ADC Interrupts are Enabled (ADIE=1)
	;Set Prescaler CK/128 = 62.5Khz (ADPS2:0=111)
	ldi r24,(1<<ADEN)|(1<<ADIE)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0)
	out ADCSRA,r24
	ret

usart_init: 
	clr r24					; initialize UCSRA to zero	 
	out UCSRA ,r24 
	ldi r24 ,(1<<RXEN) | (1<<TXEN)		; activate transmitter/receiver 
	out UCSRB ,r24 
	ldi r24 ,0					; baud rate = 9600 
	out UBRRH ,r24 
	ldi r24 ,51 
	out UBRRL ,r24 
	ldi r24 ,(1 << URSEL) | (3 << UCSZ0)	; 8-bit character size, 
	out UCSRC ,r24				; 1 stop bit 
	ret 
	
	; Routine: usart_transmit 
	; Description: 
	; This routine sends a byte of data 
	; using usart. 
	; parameters: 
	; r24: the byte to be transmitted 
	; must be stored here. 
	; return value: None. 
	; registers affected: r24 
	; routines called: None.

usart_transmit: 
	sbis UCSRA ,UDRE					; check if usart is ready to transmit 
	rjmp usart_transmit					; if no check again, else transmit 
	out UDR ,r24						; content of r24 
	ret

wait_usec:
	sbiw r24 ,1		; 2 cycles (0.250 micro sec)
	nop			; 1 cycles (0.125 micro sec)
	nop			; 1 cycles (0.125 micro sec)
	nop			; 1 cycles (0.125 micro sec)
	nop			; 1 cycles (0.125 micro sec)
	brne wait_usec	; 1 or 2 cycles (0.125 or 0.250 micro sec)
	ret			; 4 cycles (0.500 micro sec)

wait_msec:
	push r24		; 2 cycles (0.250 micro sec)
	push r25		; 2 cycles
	ldi r24 , 0xe6	; load register r25:r24 with 998 (1 cycles - 0.125 micro sec)
	ldi r25 , 0x03	; 1 cycles (0.125 micro sec)
	rcall wait_usec	; 3 cycles (0.375 micro sec), total delay 998.375 micro sec
	pop r25		; 2 cycles (0.250 micro sec)
	pop r24		; 2 cycles
	sbiw r24 , 1		; 2 cycles
	brne wait_msec	; 1 or 2 cycles (0.125 or 0.250 micro sec)
	ret

//matrixes for the voltage conversion Vin=(ADC/1024)*5

a1:
.db '0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4','4'

a2:
.db '0','0','0','0','0','0','1','1','1','1','1','2','2','2','2','2','3','3','3','3','3','4','4','4','4','4','5','5','5','5','5','6','6','6','6','6','7','7','7','7','7','8','8','8','8','8','8','9','9','9','9','9','0','0','0','0','0','1','1','1','1','1','2','2','2','2','2','3','3','3','3','3','4','4','4','4','4','5','5','5','5','5','6','6','6','6','6','6','7','7','7','7','7','8','8','8','8','8','9','9','9','9','9','0','0','0','0','0','1','1','1','1','1','2','2','2','2','2','3','3','3','3','3','4','4','4','4','4','5','5','5','5','5','5','6','6','6','6','6','7','7','7','7','7','8','8','8','8','8','9','9','9','9','9','0','0','0','0','0','1','1','1','1','1','2','2','2','2','2','3','3','3','3','3','3','4','4','4','4','4','5','5','5','5','5','6','6','6','6','6','7','7','7','7','7','8','8','8','8','8','9','9','9','9','9','0','0','0','0','0','1','1','1','1','1','1','2','2','2','2','2','3','3','3','3','3','4','4','4','4','4','5','5','5','5','5','6','6','6','6','6','7','7','7','7','7','8','8','8','8','8','9','9','9','9','9'

a3:
.db '0','1','3','5','7','9','1','3','5','7','9','1','3','5','7','9','1','3','5','7','9','1','2','4','6','8','0','2','4','6','8','0','2','4','6','8','0','2','4','6','8','0','2','3','5','7','9','1','3','5','7','9','1','3','5','7','9','1','3','5','7','9','1','3','5','6','8','0','2','4','6','8','0','2','4','6','8','0','2','4','6','8','0','2','4','6','7','9','1','3','5','7','9','1','3','5','7','9','1','3','5','7','9','1','3','5','7','8','0','2','4','6','8','0','2','4','6','8','0','2','4','6','8','0','2','4','6','8','0','1','3','5','7','9','1','3','5','7','9','1','3','5','7','9','1','3','5','7','9','1','2','4','6','8','0','2','4','6','8','0','2','4','6','8','0','2','4','6','8','0','2','3','5','7','9','1','3','5','7','9','1','3','5','7','9','1','3','5','7','9','1','3','5','6','8','0','2','4','6','8','0','2','4','6','8','0','2','4','6','8','0','2','4','6','7','9','1','3','5','7','9','1','3','5','7','9','1','3','5','7','9','1','3','5','7','8','0','2','4','6','8','0','2','4','6','8','0','2','4','6','8','0','2','4','6','8'
```

### (β) Κώδικας σε c

Οι συναρτήσεις επικοινωνίας uart και adc είναι αντίστοιχες με τις δοσμένες υπορουτίνες σε assembly. Αρχικά διαβάζεται ο αριθμός από
την uart. Στη συνέχεια γίνεται μετατροπή στην τάση βάσει του τύπου Vin=(ADC/1024)*5. Πρώτα το κάνουμε *500 και μετά /100 για ευκολία.
Για ακρίβεια 2 δεκαδικών ψηφίων γίνονται κατάλληλα mod. Χρησιμοποιούνται χαρακτήρες αλλαγής σειράς ‘\n’ ώστε να είναι τα αποτελέσματα 
ευανάγνωστα.

```c
#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <stdint.h>

#define USART_BAUDRATE 9600
#define BAUD_PRESCALE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)

void USART_init(void)
{   
    UCSRA = 0;
    UCSRB = (1<<RXEN) | (1<<TXEN);
    UBRRH = 0;
    UBRRL = 51;
    UCSRC = (1 << URSEL) | (3 << UCSZ0);
}

unsigned char USART_receive(void){
								// check if usart received a byte.
	while((UCSRA & 0x80) == 0);	//		// if no check again              //RXC is the bit7 of UCSRA
	return UDR;						// else return the received byte 
	
}

void USART_transmit(char input){
								// check if usart is ready to transmit.
	while((UCSRA & 0x20) == 0);			// if no check again                      //UDRE is the bit5 of UCSRA
	UDR = input;						// else transmit the input byte to usart
	
}

void ADC_init ()
{
    ADMUX = (1 << REFS0);
    ADCSRA = (1 << ADEN) | (1 << ADPS0) | (1 << ADPS1) | (1 << ADPS2);
}

int adc_read()
{
	ADCSRA = ADCSRA | 0x40;           //conversion starts when ADSC bit(bit 6 of ADCSRA) becomes 1

	while ( !(ADCSRA & 0x40) ) { /* WAIT UNTIL CONVERSION IS DONE */ }
        
	return ADC;
}

int main (void)
{
    /*
     ADCSRA [ADEN|ADSC|ADATE|ADIF|ADIE|*|*|*]
    */
    
    // Left Adjust Result: No
    // ADLAR = 0;
    
    // Initialize USART
    USART_init();

    // Initialize ADC
    ADC_init();


    while (1) {
	    // To start ADC conversion ADSC=1
        uint32_t res = (uint32_t)adc_read();           //read from UART
		res = res *500/1024;                           //voltage conversion given that Vcc=5
        USART_transmit ( res/100 + '0' );               //msdigit before .
        USART_transmit('.');
		USART_transmit((res/10)%10+'0');              //first digit after .
		USART_transmit(res%10+'0');                    //second digit after .
        USART_transmit('\n');                         //new line
		USART_transmit('\r');
        
        _delay_ms(200);                    //delay 200ms
        
	}
}
```