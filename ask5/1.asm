.include "m16def.inc"
main:
    ldi r24, low(RAMEND) 
    out SPL, r24
    ldi r24, high(RAMEND)
    out SPH, r24

out_loop:
    ldi zl,LOW(msg*2)
	ldi zh,HIGH(msg*2)
	
	rcall usart_init
	
	ldi r25,14
loop:
	; lpm r24,Z
    ldi r24,'#'
	rcall usart_transmit
    dec r25
    brne loop
    rjmp out_loop

msg:
.db 'G','e','i','a',' ','s','o','u',' ','k','o','s','m','e','\n'

; Routine: usart_init
; Description:
; This routine initializes the
; usart as shown below.
;------- INITIALIZATIONS -------
; Baud rate: 9600 (Fck= 8MH)
; Asynchronous mode
; Transmitter on
; Reciever on
; Communication parameters: 8 Data ,1 Stop , no Parity
;--------------------------------
; parameters: None.
; return value: None.
; registers affected: r24
; routines called: None
usart_init:
clr r24
out UCSRA ,r24
ldi r24 ,(1<<RXEN) | (1<<TXEN)
out UCSRB ,r24
ldi r24 ,0
out UBRRH ,r24
ldi r24 ,51
out UBRRL ,r24
ldi r24 ,(1 << URSEL) | (3 << UCSZ0)
out UCSRC ,r24
ret

; activate transmitter/receiver
; baud rate = 9600
; 8-bit character size,
; 1 stop bit
; Routine: usart_transmit
; Description:
; This routine sends a byte of data
; using usart.
; parameters:
; r24: the byte to be transmitted
; must be stored here.
; return value: None.
; registers affected: r24
; routines called: None.
usart_transmit:
sbis UCSRA ,UDRE
rjmp usart_transmit
out UDR ,r24
ret

; initialize UCSRA to zero
; check if usart is ready to transmit
; if no check again, else transmit
; content of r24
;Routine: usart_receive
;Description:
;This routine receives a byte of data
;from usart.
;parameters: None.;
;return value: the received byte is
;returned in r24.
;registers affected: r24
;routines called: None.

;check if usart received byte
;if no check again, else read
;receive byte and place it in
;r24

usart_receive:
sbis UCSRA ,RXC
rjmp usart_receive
in r24 ,UDR
ret

;Routine: usart_init
;Description:
;This routine initializes the
;ADC as shown below.
;------- INITIALIZATIONS -------
;Vref: Vcc (5V for easyAVR6)
;Selected pin is A0
;ADC Interrupts are Enabled
;Prescaler is set as CK/128 = 62.5kHz
;--------------------------------
;parameters: None.
;return value: None.
;registers affected: r24
;routines called: None
ADC_init:
ldi r24,(1<<REFS0) ; Vref: Vcc
out ADMUX,r24
;MUX4:0 = 00000 for A0.
;ADC is Enabled (ADEN=1)
;ADC Interrupts are Enabled (ADIE=1)
;Set Prescaler CK/128 = 62.5Khz (ADPS2:0=111)
ldi r24,(1<<ADEN)|(1<<ADIE)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0)
out ADCSRA,r24
ret
