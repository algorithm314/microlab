void usart_init()
{
    UCSRA=0;
    UCSRB= (1<<RXEN)|(1<<TXEN);
    UBRRH=0;
    UBRRL=51;     // baud rate = 9600  for 8MHZ
    UCSRC=(1<<URSEL)|(3<<UCSZ0);
}


void  usart_transmit(char UDRE_tr)
{
    while(!(UCSRA & (1<<UDRE)));
    UDR=UDRE_tr;
    
}

char usart_receive()
{
    while(!(UCSRA & (1<<RXC)));
    return UDR;
    
    
}

void ADC_init()
{
    ADMUX=(1<<REFS0);
    ADCSRA=(1<<ADEN)|(1<<ADIE)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
    
}


