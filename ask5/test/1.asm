.include "m16def.inc"
.org 0x00

.CSEG
rjmp main

Message1:
.db "Geia sou kosme!", '\n','\r', '\0'			; Write a string in ram
main:
	ldi ZL, low(Message1<<1)		; Initialize Z pointer in order to point to
	ldi ZH, high(Message1<<1)		; the first element of the string
						; which is basically an array of characters

set_stack:					; Initialize Stack
	ldi r24, low(RAMEND)
	out SPL, r24
	ldi r24, high(RAMEND)
	out SPH, r24

print_strint_to_uart:

     rcall usart_init			; Initialize usart
 
	 loop_:
		lpm r24, Z+			; Load the context of where Z points to r24 

		cpi r24, '\0'			; If you reach the end of string go to next_ branch
		breq main

		rcall usart_transmit		; Transmit to usart the context of r24
		
		push r24			; Save the value of r24 on stack

		ldi r24,low(100)		; Wait 100 msec for the results		
		ldi r25,high(100)		; to be presented more clearly
		rcall wait_msec

		pop r24			; Restore the value of r24
		
		rjmp loop_

	; ------------------------ ;
	; ------- ROUTINES ------- ;
	; ------------------------ ;

	; Routine: usart_init 
	; Description: 
	; This routine initializes the 
	; usart as shown below. 
	; ------- INITIALIZATIONS ------- 
	; 
	; Baud rate: 9600 (Fck = 8MHz) 
	; Asynchronous mode 
	; Transmitter on 
	; Reciever on 
	; Communication parameters: 8 Data ,1 Stop , no Parity 
	; -------------------------------- 
	; parameters: None. 
	; return value: None. 
	; registers affected: r24 
	; routines called: None

	usart_init: 
		clr r24					; initialize UCSRA to zero	 
		out UCSRA ,r24 
		ldi r24 ,(1<<RXEN) | (1<<TXEN)		; activate transmitter/receiver 
		out UCSRB ,r24 
		ldi r24 ,0					; baud rate = 9600 
		out UBRRH ,r24 
		ldi r24 ,51 
		out UBRRL ,r24 
		ldi r24 ,(1 << URSEL) | (3 << UCSZ0)	; 8-bit character size, 
		out UCSRC ,r24				; 1 stop bit 
		ret 
	
	; Routine: usart_transmit 
	; Description: 
	; This routine sends a byte of data 
	; using usart. 
	; parameters: 
	; r24: the byte to be transmitted 
	; must be stored here. 
	; return value: None. 
	; registers affected: r24 
	; routines called: None.

usart_transmit: 
	sbis UCSRA ,UDRE					; check if usart is ready to transmit 
	rjmp usart_transmit					; if no check again, else transmit 
	out UDR ,r24						; content of r24 
	ret 
	
	; Routine: usart_receive 
	; Description: 
	; This routine receives a byte of data 
	; from usart. 
	; parameters: None. 
	; return value: the received byte is 
	; returned in r24. 
	; registers affected: r24 
	; routines called: None. 
	
wait_usec:
	sbiw r24 ,1		; 2 cycles (0.250 micro sec)
	nop			; 1 cycles (0.125 micro sec)
	nop			; 1 cycles (0.125 micro sec)
	nop			; 1 cycles (0.125 micro sec)
	nop			; 1 cycles (0.125 micro sec)
	brne wait_usec	; 1 or 2 cycles (0.125 or 0.250 micro sec)
	ret			; 4 cycles (0.500 micro sec)

wait_msec:
	push r24		; 2 cycles (0.250 micro sec)
	push r25		; 2 cycles
	ldi r24 , 0xe6	; load register r25:r24 with 998 (1 cycles - 0.125 micro sec)
	ldi r25 , 0x03	; 1 cycles (0.125 micro sec)
	rcall wait_usec	; 3 cycles (0.375 micro sec), total delay 998.375 micro sec
	pop r25		; 2 cycles (0.250 micro sec)
	pop r24		; 2 cycles
	sbiw r24 , 1		; 2 cycles
	brne wait_msec	; 1 or 2 cycles (0.125 or 0.250 micro sec)
	ret
