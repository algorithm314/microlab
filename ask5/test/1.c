#include <avr/io.h> 
#include <stdio.h>

//Declaration of functions
void USART_init(void);					
unsigned char USART_receive(void);		
void USART_transmit(char);				
void USART_putstring(char*);			

int main(void){  
	
	//Declaration of variables
	char check,output;
	char msg1[] = "Invalid Number\n\r" ;
	char msg2[] = "Read " ;
	char msg3[] = "\n\r" ;
	
	DDRB = 0xFF;						// set PORTC as output
	
	USART_init();						// initialize usart
	
	while (1){
		
		check = USART_receive();	
		if ( (check < '0') || (check > '8') ){  	// if the byte is not a number			{						// between 0 and 8
			USART_putstring(msg1);		// print InvalidNumber, change line
			continue;				// and go through while loop from the top
		}
		//char c;
		//while((c = USART_receive()) != \n);
		USART_putstring(msg2);			// else print ReadX , 
		USART_transmit(check);			// which X is the number we received
		USART_putstring(msg3);			// and change line
		
		output = check - '0';			// transform ascii to value(HEX)
		PORTB = (1<<output)-1;				// light the led suited to input number

	}

	return 0;                  
}
void USART_init(void){
	
	UCSRA = 0;						// initialize UCSRA to zero
	UCSRB = (1<<RXEN) | (1<<TXEN);			// activate transmitter/receiver 
	UBRRH = 0;						// baud rate = 9600 
	UBRRL = 51;
	UCSRC = (1 << URSEL) | (3 << UCSZ0);		// 8-bit character size, 1 stop bit

}

unsigned char USART_receive(void){
								// check if usart received a byte.
	while((UCSRA & 0x80) == 0);			// if no check again 
	return UDR;						// else return the received byte 
	
}

void USART_transmit(char input){
								// check if usart is ready to transmit.
	while((UCSRA & 0x20) == 0);			// if no check again
	UDR = input;						// else transmit the input byte to usart
	
}

void USART_putstring(char* StringPtr){			// this function is used to print 
								// a string to usart
	while(*StringPtr != '\0')				// if you haven't reached the end of string
		USART_transmit(*StringPtr++);			// transit the current character of the string
}

