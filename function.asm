.include "m16def.inc"

main:
ldi r16,high(RAMEND) ; ldi can use only r16-r31
out SPH,r16
ldi r16,low(RAMEND)
out SPL,r16

; DDR output 1
; DDR input 0

ser r20
out DDRC, r20 ;output

clr r20
out DDRD, r20 ;input

loop:
in r20,PIND
mov r21,r20
mov r22,r20
mov r23,r20

lsr r20
eor r22,r20
lsr r20
eor r22,r20
lsr r20
eor r22,r20

lsl r22
andi r22,2 ;r22 has the first msb 

mov r20,r23
lsr r23
and r20,r23

lsl r23
lsl r23
and r21,r23
lsr r21
lsr r21
lsr r21


or r20,r21
andi r20,1

or r20,r22

out PORTC,r20

rjmp loop
