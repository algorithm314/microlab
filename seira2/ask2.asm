.include "m16def.inc"

.org 0x0
rjmp reset
.org 0x2 ; EIO0 routine
rjmp INT0_ROUT
;.org 0x4
;rjmp INT1_ROUT

reset:
	ldi r16,high(RAMEND) ; ldi can use only r16-r31
	out SPH,r16
	ldi r16,low(RAMEND)
	out SPL,r16

	; positive edge
	ldi r24,(1 << ISC01)|(1 << ISC00)
	out MCUCR, r24
	; enable INT0
	ldi r24,(1 << INT0)
	out GICR, r24
	sei

	; main start

ser r26
out DDRC, r26 ; PORTB output
clr r26; count = 0
out DDRA, r26


inf_loop:
	rjmp inf_loop
	; main end


INT0_ROUT:
	push r26
	in r26, SREG
	push r26

;;;;;;;;;;;;;;;;;;;;;
	push r24
	push r23


	; SPARKS
spark_loop:
	ldi r24 ,(1 << INTF0)
	out GIFR ,r24
	ldi r24 , low(5)
	ldi r25 , high(5)
	rcall wait_msec
	in r24, GIFR
	andi r24, (1 << INTF0)
	brne spark_loop
	;END SPARKS



	in r24, PINA
	; result is in r23
	clr r23
loop:
	sbrs r24,7
	rjmp next
	lsl r23
	ori r23,1
next:
	lsl r24
	cpi r24,0
	brne loop

	out PORTC,r23
	pop r23
	pop r24
;;;;;;;;;;;;;;;;;;;;;

	pop r26; missing from example??
	out SREG, r26
	pop r26
	reti

wait_msec:
push r24 ; 2 eyeeie (0.250 isec)
push r25 ; 2 eyeeie
ldi r24 , low(998) ; ounouoa oii eaoa?. r25:r24 ia 998 (1 eyeeio - 0.125 isec)
ldi r25 , high(998) ; 1 eyeeio (0.125 isec)
rcall wait_usec ; 3 eyeeie (0.375 isec), ?nieaea? ooiieeeU eaeoooYncoc 998.375 isec
pop r25 ; 2 eyeeie (0.250 isec)
pop r24 ; 2 eyeeie
sbiw r24 , 1 ; 2 eyeeie
brne wait_msec ; 1 ? 2 eyeeie (0.125 ? 0.250 isec)
ret ; 4 eyeeie (0.500 isec)
wait_usec:
sbiw r24 ,1 ; 2 eyeeie (0.250 isec)
nop ; 1 eyeeio (0.125 isec)
nop ; 1 eyeeio (0.125 isec)
nop ; 1 eyeeio (0.125 isec)
nop ; 1 eyeeio (0.125 isec)
brne wait_usec ; 1 ? 2 eyeeie (0.125 ? 0.250 isec)
ret ;

