---
mainfont: Open Sans
documentclass: extarticle
fontsize: 12pt
geometry: margin=1in
papersize: a4
title: "2η Σειρά ασκήσεων, Εργαστήριο Μικροϋπολογιστών"
author:
- "Αγιάννης Κωνσταντίνος 03116352"
- "Τσιαχρίστος Δημήτριος 03116048"
---

# Ζήτημα 2.1 

Με την ldi r24,(1 << ISC11)|(1 << ISC10) ως sei ορίζουμε τη διακοπή INT1. Στο loop: η κάθε μέτρηση αργεί 200ms. Ελέγχουμε ότι το PD7 είναι 1. Με το spark_loop: 
γίνονται οι σπινθηρισμοί.


```asm
.include "m16def.inc"

.cseg

.org 0x0
rjmp reset
;.org 0x2 ; ÉÍÔ0 routine
;rjmp INT0_ROUT
.org 0x4
rjmp INT1_ROUT

reset:
	ldi r16,high(RAMEND) ; ldi can use only r16-r31
	out SPH,r16
	ldi r16,low(RAMEND)
	out SPL,r16

	; positive edge
	ldi r24,(1 << ISC11)|(1 << ISC10)   ;INT1 interrupt 
	out MCUCR, r24
	; enable INT1
	ldi r24,(1 << INT1)
	out GICR, r24
	sei   ;enable interrupts

	; main start

ser r26
out DDRB, r26 ; PORTB output
out DDRA, r26 ; PORTA output
clr r26; count = 0
out DDRD, r26

ldi r30,low(0x100)
ldi r31,high(0x100)
st Z,r26 ; clear counter

loop:
	out PORTB, r26
	ldi r24 ,low(200)
	ldi r25 ,high(200) 
	rcall wait_msec ; delay 200 ms
	inc r26; count++
	rjmp loop

	; main end

INT1_ROUT:
	push r26
	in r26, SREG
	push r26
	
	in r26, PIND

	andi r26, 1<<7
	breq end ; if zero: goto end

	; main start
	push r24
	push r25
	; SPARKS
spark_loop:
	ldi r24 ,(1 << INTF1); maybe intF1???????
	out GIFR ,r24
	ldi r24 , low(5)
	ldi r25 , high(5)
	rcall wait_msec
	in r24, GIFR
	andi r24, (1 << INTF1)
	brne spark_loop
	;END SPARKS


	; load counter
	ldi r30,low(0x100)
	ldi r31,high(0x100)
	ld r18,Z
	
	inc r18
	out PORTA,r18
	; save counter
	st Z,r18
	
	pop r25
	pop r24
	; main end
end:
	pop r26; missing from example??
	out SREG, r26
	pop r26
	reti

wait_msec:
	;ret ;DEGUG
	push r24
	push r25
	ldi r24 , low(998) 
	ldi r25 , high(998) 
	rcall wait_usec 
	pop r25 
	pop r24 
	sbiw r24 , 1 
	brne wait_msec 
	ret 
wait_usec:
	sbiw r24 ,1 
	nop 
	nop 
	nop 
	nop 
	brne wait_usec 
	ret 


.dseg
.org 0x100
counter:
	.byte 1 ; reserve 1 byte

```
# Ζήτημα 2.2


Στο org 0x2 ορίζουμε θέση για τη διακοπή INT0. Στο reset ορίζεται διακοπή INT0. Κάθε φορά που το msb της εισόδου Α είναι 1 θέσε έναν ακόμα άσσο 
στο πλέον lsb της εξόδου C (για άναμμα led).(θα μπορούσε να γίνει και αλλιώς μετρώντας πρώτα το πλήθος    όλων των άσσων στην Α και μετά να θέτει τόσα στη C ). Υπάρχουν σπινθηρισμοί. 



```asm
.include "m16def.inc"

.org 0x0
rjmp reset
.org 0x2 ; EIO0 routine
rjmp INT0_ROUT
;.org 0x4
;rjmp INT1_ROUT

reset:
	ldi r16,high(RAMEND) ; ldi can use only r16-r31
	out SPH,r16
	ldi r16,low(RAMEND)
	out SPL,r16

	; positive edge
	ldi r24,(1 << ISC01)|(1 << ISC00) ;INTO interrupt
	out MCUCR, r24
	; enable INT0
	ldi r24,(1 << INT0)
	out GICR, r24
	sei

	; main start

ser r26
out DDRC, r26 ; PORTC output
clr r26; count = 0
out DDRA, r26


inf_loop:
	rjmp inf_loop    ;perimenei gia kapoio interrupt
	; main end


INT0_ROUT:
	push r26
	in r26, SREG
	push r26

;;;;;;;;;;;;;;;;;;;;;
	push r24
	push r23


	; SPARKS
spark_loop:
	ldi r24 ,(1 << INTF0)
	out GIFR ,r24
	ldi r24 , low(5)
	ldi r25 , high(5)
	rcall wait_msec
	in r24, GIFR
	andi r24, (1 << INTF0)
	brne spark_loop
	;END SPARKS



	in r24, PINA  ;input
	; result is in r23
	clr r23
loop:
	sbrs r24,7   ;an to msb set kane olisthisi kai prosthese akoma enan 1 sto telos sto apotelesma
	rjmp next
	lsl r23       ;aristeri olisthisi 
	ori r23,1
next:
	lsl r24
	cpi r24,0
	brne loop

	out PORTC,r23 ;output
	pop r23
	pop r24
;;;;;;;;;;;;;;;;;;;;;

	pop r26; missing from example??
	out SREG, r26
	pop r26
	reti   ;exodos yporoutinas

wait_msec:
push r24 ; 2 eyeeie (0.250 isec)
push r25 ; 2 eyeeie
ldi r24 , low(998) ; ounouoa oii eaoa?. r25:r24 ia 998 (1 eyeeio - 0.125 isec)
ldi r25 , high(998) ; 1 eyeeio (0.125 isec)
rcall wait_usec ; 3 eyeeie (0.375 isec), ?nieaea? ooiieeeU eaeoooYncoc 998.375 isec
pop r25 ; 2 eyeeie (0.250 isec)
pop r24 ; 2 eyeeie
sbiw r24 , 1 ; 2 eyeeie
brne wait_msec ; 1 ? 2 eyeeie (0.125 ? 0.250 isec)
ret ; 4 eyeeie (0.500 isec)
wait_usec:
sbiw r24 ,1 ; 2 eyeeie (0.250 isec)
nop ; 1 eyeeio (0.125 isec)
nop ; 1 eyeeio (0.125 isec)
nop ; 1 eyeeio (0.125 isec)
nop ; 1 eyeeio (0.125 isec)
brne wait_usec ; 1 ? 2 eyeeie (0.125 ? 0.250 isec)
ret ;
```

# Ζήτημα 2.3

Αρχικά ορίζουμε τους χρόνους διακοπής σήματος υπερχείλισης (0.5sec για PB0-PB7 και 3.5sec ακόμα για το PB0) για 8MHZ συχνότητα όπως προκύπτουν από παρόμοιο παράδειγμα.
Αλλαγές γίνονται με το πάτημα του PD3 ή του PA7 με προτεραιότητα στο PD3 καθώς αντιστοιχεί σε διακοπή που δρα
πάνω από όλα τ' άλλα. Στο timer_start έχουμε συχνότητα αύξησης του Χρονιστή TCNT1 ίση με τη 1/1024 του  AVR.


## Κώδικας σε ASM

```asm
.include "m16def.inc"

#define sec4  31250       ;orismoi shmaton diakopis yperxeilisis
#define sec35 38192
#define sec05 61630
#define led r28

; 0 nothing
; 1  -> 3.5 state     ;to B0 thelei akoma 3.5sec anammeno
; 2  -> 0.5 state

; 3.5* 7812.5 = 27343.75
; 65536 - 27343.75 = 38192.25
; 0.5 = 61629.75

.cseg

.org 0x0
rjmp reset
;.org 0x2 ; INT0 routine
;rjmp INT0_ROUT
.org 0x4
rjmp INT1_ROUT
.org 0x10
rjmp TIMER_ROUT

reset:
	ldi r16,high(RAMEND) ; ldi can use only r16-r31
	out SPH,r16
	ldi r16,low(RAMEND)
	out SPL,r16


	
	; positive edge
	ldi r24,(1 << ISC11)|(1 << ISC10)  ;INT1 interrupt
	out MCUCR, r24
	; enable INT1
	ldi r24,(1 << INT1)
	out GICR, r24
	sei

	clr r0 ; r0 = 0
	clr r1
	dec r1 ; r1 = all 1
	clr r2
	inc r2 ; r2 = 1
	clr led


	out DDRA,r0
	out DDRB,r1
	
	rcall timer_start

pa_loop:
	sbis PINA,7      ;oso to PA7 sbisto kane to loop
	rjmp pa_loop

rcall open_leds

wait_for_0:
sbic PINA,7          ;oso einai patimeno to PA7 tipota-allagi sto afima
rjmp wait_for_0

rjmp pa_loop


INT1_ROUT:
	
	rcall open_leds
	reti

TIMER_ROUT:
	push r24
	cpi led,1
	brne next
	; 3.5 state
	out PORTB,r2 ; 1 anapse to PB0
	ldi led, 2
	ldi r24, high(sec35)  ;sima diakopis yperxeilisis 3.5sec gia to PB0
	out TCNT1H ,r24
	ldi r24, low(sec35)
	out TCNT1L,r24
	rjmp timer_end
next:
	cpi led, 2
	brne timer_end
	out PORTB,r0; 0
	clr led
	; stop timer

	out TCCR1B, r0 ;0

timer_end:
	pop r24
	reti

open_leds:
	push r24
	push r16
	rcall timer_start
	cpi led, 0
	breq led_0


	ldi led,1
	ldi r24, high(sec05)     ;sima diakopis yperxeilisis 0.5sec gia ta PB0-PB7
	out TCNT1H ,r24
	ldi r24, low(sec05)
	out TCNT1L,r24
	out PORTB,r1     ; -1 (ola 1) anapse PB0-PB7
	rjmp led_end
led_0:
	ldi led,2
	ldi r24, high(sec4)     ;sima diakopis yperxeilisis 4sec gia to PB0
	out TCNT1H ,r24
	ldi r24, low(sec4)
	out TCNT1L,r24
	out PORTB,r2 ; 1  anapse PB0
	
led_end:
	pop r16	
	pop r24
	ret

timer_start:
	push r24
	ldi r24 ,(1<<CS12) | (0<<CS11) | (1<<CS10) ; CK/1024
	out TCCR1B ,r24
	
	ldi r24 ,(1<<TOIE1) ; enable TCNT1
	out TIMSK ,r24
	pop r24
	ret
```

## Κώδικας σε C

```c
#include <avr/io.h>
#include <avr/interrupt.h>
//#include <util/delay.h>

static volatile char led = 0; //vollatile for variables accessed from interrupts
// led 0 no open led
// led 1 0.5 phase
// led 2 3.5 or 4 phase

void timer_start()
{
	TCCR1B = (1<<CS12) | (0<<CS11) | (1<<CS10);  //  CK/1024
	TIMSK  = (1<<TOIE1);
}

void timer_stop()
{
	TCCR1B = 0;
}

void update_leds()
{
#define sec4  31250      //orismoi shmaton diakopis yperxeilisis
#define sec35 38192
#define sec05 61630
	timer_start();
	if(led){
		PORTB = 0xff;    //anapse PB0-PB7
		led = 1;
		TCNT1 = sec05;    //gia 0.5sec
	}else{
		PORTB = 1;     //anapse PB0
		led = 2;
		TCNT1 = sec4;  //gia 4sec
	}
}

ISR(TIMER1_OVF_vect)
{
	//PORTB = 0;
	//timer_stop();
	if(led == 1){
		PORTB = 1;
		led = 2;
		TCNT1 = sec35;
	}else if(led == 2){
		PORTB = 0;
		led = 0;
		timer_stop();
	}
}

ISR (INT1_vect)
{
	update_leds();
}

int main()
{
	DDRB = 0xff; //output
	DDRA = 0x00; //input
	//enable INT1
	GICR =(1<<INT1);
	MCUCR = (1 << ISC11)|(1 << ISC10);
	sei();    //enable interrupts
	//enable timer
	timer_start();
	while(1){
		
		if((PINA & 128) != 0){   ;otan patithi to PA7
			update_leds();
			while((PINA & 128) != 0);  ;oso einai patimeno to PA7 tipota-allagi sto afima
		}
	}
}
```
