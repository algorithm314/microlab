.include "m16def.inc"

.org 0x0
rjmp reset
.org 0x2 ; INT0 routine
rjmp INT0_ROUT
;.org 0x4
;rjmp INT1_ROUT

reset:
	ldi r1,high(REMEND)
	out SPH,r1
	ldi r1,low(REMEND)
	out SPL,r1
	; positive edge
	ldi r24,(1 << ISC01)|(1 << ISC00)
	out MCUCR, r24
	; enable INT0
	ldi r24,(1 << INT0)
	out GICR, r24
	sei

	; main start


	; main end

INT0_ROUT:
	push r26
	in r26, SREG
	push r26

	; main start
	; main end

	pop r26; missing from example??
	out SREG, r26
	pop r26
	reti
