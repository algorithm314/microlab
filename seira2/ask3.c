#include <avr/io.h>
#include <avr/interrupt.h>
//#include <util/delay.h>

static volatile char led = 0; //vollatile for variables accessed from interrupts
// led 0 no open led
// led 1 0.5 phase
// led 2 3.5 or 4 phase

void timer_start()
{
	TCCR1B = (1<<CS12) | (0<<CS11) | (1<<CS10);
	TIMSK  = (1<<TOIE1);
}

void timer_stop()
{
	TCCR1B = 0;
}

void update_leds()
{
#define sec4  31250
#define sec35 38192
#define sec05 61630
	timer_start();
	if(led){
		PORTB = 0xff;
		led = 1;
		TCNT1 = sec05;
	}else{
		PORTB = 1;
		led = 2;
		TCNT1 = sec4;
	}
}

ISR(TIMER1_OVF_vect)
{
	//PORTB = 0;
	//timer_stop();
	if(led == 1){
		PORTB = 1;
		led = 2;
		TCNT1 = sec35;
	}else if(led == 2){
		PORTB = 0;
		led = 0;
		timer_stop();
	}
}

ISR (INT1_vect)
{
	update_leds();
}

int main()
{
	DDRB = 0xff; //output
	DDRA = 0x00; //input
	//enable INT1
	GICR =(1<<INT1);
	MCUCR = (1 << ISC11)|(1 << ISC10);
	sei();
	//enable timer
	timer_start();
	while(1){
		
		if((PINA & 128) != 0){
			update_leds();
			while((PINA & 128) != 0);
		}
	}
}
