.include "m16def.inc"

#define sec4  31250
#define sec35 38192
#define sec05 61630
#define led r28

; 0 nothing
; 1  -> 3.5 state
; 2  -> 0.5 state

; 3.5* 7812.5 = 27343.75
; 65536 - 27343.75 = 38192.25
; 0.5 = 61629.75

.cseg

.org 0x0
rjmp reset
;.org 0x2 ; INT0 routine
;rjmp INT0_ROUT
.org 0x4
rjmp INT1_ROUT
.org 0x10
rjmp TIMER_ROUT

reset:
	ldi r16,high(RAMEND) ; ldi can use only r16-r31
	out SPH,r16
	ldi r16,low(RAMEND)
	out SPL,r16


	
	; positive edge
	ldi r24,(1 << ISC11)|(1 << ISC10)
	out MCUCR, r24
	; enable INT1
	ldi r24,(1 << INT1)
	out GICR, r24
	sei

	clr r0 ; r0 = 0
	clr r1
	dec r1 ; r1 = all 1
	clr r2
	inc r2 ; r2 = 1
	clr led


	out DDRA,r0
	out DDRB,r1
	
	rcall timer_start

pa_loop:
	sbis PINA,7
	rjmp pa_loop

rcall open_leds

wait_for_0:
sbic PINA,7
rjmp wait_for_0

rjmp pa_loop


INT1_ROUT:
	
	rcall open_leds
	reti

TIMER_ROUT:
	push r24
	cpi led,1
	brne next
	; 3.5 state
	out PORTB,r2 ; 1
	ldi led, 2
	ldi r24, high(sec35)
	out TCNT1H ,r24
	ldi r24, low(sec35)
	out TCNT1L,r24
	rjmp timer_end
next:
	cpi led, 2
	brne timer_end
	out PORTB,r0; 0
	clr led
	; stop timer

	out TCCR1B, r0 ;0

timer_end:
	pop r24
	reti

open_leds:
	push r24
	push r16
	rcall timer_start
	cpi led, 0
	breq led_0


	ldi led,1
	ldi r24, high(sec05)
	out TCNT1H ,r24
	ldi r24, low(sec05)
	out TCNT1L,r24
	out PORTB,r1 ; -1
	rjmp led_end
led_0:
	ldi led,2
	ldi r24, high(sec4)
	out TCNT1H ,r24
	ldi r24, low(sec4)
	out TCNT1L,r24
	out PORTB,r2 ; 1
	
led_end:
	pop r16	
	pop r24
	ret

timer_start:
	push r24
	ldi r24 ,(1<<CS12) | (0<<CS11) | (1<<CS10) ; CK/1024
	out TCCR1B ,r24
	
	ldi r24 ,(1<<TOIE1) ; enable TCNT1
	out TIMSK ,r24
	pop r24
	ret

