.include "m16def.inc"

.cseg

.org 0x0
rjmp reset
;.org 0x2 ; ÉÍÔ0 routine
;rjmp INT0_ROUT
.org 0x4
rjmp INT1_ROUT

reset:
	ldi r16,high(RAMEND) ; ldi can use only r16-r31
	out SPH,r16
	ldi r16,low(RAMEND)
	out SPL,r16

	; positive edge
	ldi r24,(1 << ISC11)|(1 << ISC10)
	out MCUCR, r24
	; enable INT1
	ldi r24,(1 << INT1)
	out GICR, r24
	sei

	; main start

ser r26
out DDRB, r26 ; PORTB output
out DDRA, r26 ; PORTA output
clr r26; count = 0
out DDRD, r26

ldi r30,low(0x100)
ldi r31,high(0x100)
st Z,r26 ; clear counter

loop:
	out PORTB, r26
	ldi r24 ,low(200)
	ldi r25 ,high(200) 
	rcall wait_msec ; delay 200 ms
	inc r26; count++
	rjmp loop

	; main end

INT1_ROUT:
	push r26
	in r26, SREG
	push r26
	
	in r26, PIND

	andi r26, 1<<7
	breq end ; if zero: goto end

	; main start
	push r24
	push r25
	; SPARKS
spark_loop:
	ldi r24 ,(1 << INTF1); maybe intF1???????
	out GIFR ,r24
	ldi r24 , low(5)
	ldi r25 , high(5)
	rcall wait_msec
	in r24, GIFR
	andi r24, (1 << INTF1)
	brne spark_loop
	;END SPARKS


	; load counter
	ldi r30,low(0x100)
	ldi r31,high(0x100)
	ld r18,Z
	
	inc r18
	out PORTA,r18
	; save counter
	st Z,r18
	
	pop r25
	pop r24
	; main end
end:
	pop r26; missing from example??
	out SREG, r26
	pop r26
	reti

wait_msec:
	;ret ;DEGUG
	push r24
	push r25
	ldi r24 , low(998) 
	ldi r25 , high(998) 
	rcall wait_usec 
	pop r25 
	pop r24 
	sbiw r24 , 1 
	brne wait_msec 
	ret 
wait_usec:
	sbiw r24 ,1 
	nop 
	nop 
	nop 
	nop 
	brne wait_usec 
	ret 


.dseg
.org 0x100
counter:
	.byte 1 ; reserve 1 byte
