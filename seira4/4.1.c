#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdint.h>
//#define KEYBOARD

#define _NOP() do { __asm__ __volatile__ ("nop"); } while (0)

char t[16] = {1,2,3,0xA,4,5,6,0xB,7,8,9,0xC,0xE,0,0xF,0xD};
char to_ascii[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

int comp2_to_celcius(uint8_t msbs,uint8_t lsbs)
{
   int16_t celcius = (msbs << 8) + lsbs;
   return celcius / 2;
    
}

int one_wire_reset()
{
	DDRA |= (1 << 4);
	PORTA &= ~(1<<4);
	_delay_us(480);
	DDRA &= ~(1 << 4);
	PORTA &= ~(1<<4);
	_delay_us(100);
	char PA4_digit = PINA & 0x10;  //care only about PA4
	_delay_us(380);
	if(PA4_digit) return 0;
	else return 1;
}

int one_wire_receive_bit()
{
	DDRA |= (1 << 4);
	PORTA &~(1<<4);
	_delay_us(2);
	DDRA &= ~(1 << 4);
	PORTA &= ~(1<<4);
	_delay_us(10);
	char bit = (PINA >> 4) & 1;
 
	_delay_us(49);
	return bit;
 }
 
 int one_wire_receive_byte()
 {
	unsigned int res = 0;
	for(int i = 0;i < 8;i++)
		res = (res >> 1) | (one_wire_receive_bit() << 7);
	return res;
}
 
 void one_wire_transmit_bit(char x)
 {
	DDRA |= (1 << 4);
	PORTA &= ~(1<<4);
	_delay_us(2);
	x = x & 1;
	if(x) PORTA |= (1<<4);
	else PORTA &= ~(1<<4);
	_delay_us(58);
	DDRA &= ~(1 << DDA4);
	PORTA &= ~(1<<4);
	_delay_us(1);
}
 
void one_wire_transmit_byte(char x)
{
	for(int i=0;i<8;i++){
		one_wire_transmit_bit(x & 1);
		x >>= 1;
	}
}



//****************************************************************************

void init_keyboard()
{
    DDRC = 0xf0;
}


int msb(int x)
{
    int res = 0;
    while(x > 1){
        res++;
        x>>=1;
    }
    return res;
}

//only one key pressed at a time
//if nothing pressed: return -1
int scan_keypad()
{
    int key;
    for(int i = 4;i < 8;i++){
        PORTC = 1<<i;
        _NOP();
		_NOP();
        key = PINC & 0xf;
        if(key != 0)
            return 4*(i-4)+msb(key);
    }
    return -1;
}
int prev = -1;

int read_spark(){
	int t1,t2;
	do{
		t1 = scan_keypad();
		_delay_ms (16);
		t2 = scan_keypad();
	}while(t1 != t2);
	return t2;
}

int scan_rising_edge()
{
    do{
        int tmp;
        while((tmp = read_spark()) == prev)
            ;
        prev = tmp;
    } while(prev == -1);
	return prev;
}

void pd3_pulse()
{
    char temp = PORTD;
    PORTD = temp | (1<<3);
    PORTD = temp;
}

void write_2_nibbles(char x)
{
    char lsb4 = PORTD & 0x0f;
    PORTD = lsb4 | (x & 0xf0);
    pd3_pulse();
    PORTD = lsb4 | (x << 4);
    pd3_pulse();
}

void lcd_data(char x)
{
    PORTD = PORTD | (1<<2);
    write_2_nibbles(x);
    _delay_us(43);
}

void lcd_command(char x)
{
    PORTD = PORTD & (~(1<<2));
    write_2_nibbles(x);
    _delay_us(39);
}

void lcd_init()
{
    DDRD = 0xff;

    _delay_ms(40);
    PORTD = 0x30;
    pd3_pulse();
    _delay_us(39);
    
    PORTD = 0x30;
    pd3_pulse();
    _delay_us(39);
    
    PORTD = 0x20;
    pd3_pulse();
    _delay_us(39);
    
    lcd_command(0x28);
    lcd_command(0x0c);
    lcd_command(0x01);
    
    _delay_us(1530);
    lcd_command(0x06);
    
}

//****************************************************************
//****************************************************************

int read_temperature()
{
#ifdef KEYBOARD
	int k1 = t[scan_rising_edge()];
	int k2 = t[scan_rising_edge()];
	int k3 = t[scan_rising_edge()];
	int k4 = t[scan_rising_edge()];
	int tmp = ((k1 << 12) | (k2 << 8) | (k3 << 4) | k4);
	if(tmp != 0x8000)
		return tmp/2;
	else
		return 0x8000;
	
#else
	int temperature = 0x8000;
	
	int detect = one_wire_reset();
	if(!detect) return 0x8000;
	one_wire_transmit_byte(0xCC);
	one_wire_transmit_byte(0x44);
	// wait to complete
	while(one_wire_receive_bit());
	//lcd_data('1');
	detect = one_wire_reset();
	if(detect){
		one_wire_transmit_byte(0xCC);
		one_wire_transmit_byte(0xBE);
		uint8_t lsbits = one_wire_receive_byte();  //if it reads 8-MSB first
		uint8_t msbits = one_wire_receive_byte();
		temperature = comp2_to_celcius(msbits,lsbits);
	}
	return temperature;
	
#endif
}


int main()
{
	DDRB = 0xff; //outtput
	while(1){
		int temperature = read_temperature();
		PORTB = temperature & 0xff;
	}
}

