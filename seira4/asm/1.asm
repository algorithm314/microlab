.include "m16def.inc"
.def temp = r18

    ;Δημιουργία της Στοίβας.
    ldi r24,LOW(RAMEND)
    out SPL,r24
    ldi r25,HIGH(RAMEND)
    out SPH,r25

start:
    rcall read_temp
    ldi r24, low(1000)
    ldi r25, high(1000)
    rcall wait_msec
    rjmp start

; Ρουτίνα που επιστρέφει την μετρούμενη θερμοκρασία στο ζεύγος r25:r24
; σε δυαδική μορφή συμπληρώματος ως προς 1. Αν εμφανιστεί οποιοδήποτε σφάλμα,
; επιστρέφουμε την τιμή 8000.
read_temp:
    rcall one_wire_reset            ; Αρχικοποίηση συσκευής.
    sbrs r24, 0                     ; Αν δε βρεθεί συσκευή (r24=0),
    rjmp no_device                  ; επιστρέφουμε με τιμή 8000.

    ldi r24, 0xCC                   ; Αγνοούμε τον έλεγχο για πολλές συσκευές.
    rcall one_wire_transmit_byte
    ldi r24, 0x44                   ; Ζητάμε να ξεκινήσει η μέτρηση της
    rcall one_wire_transmit_byte    ; θερμοκρασίας.

check_finished:
    rcall one_wire_receive_bit      ; Ελέγχουμε αν έχει τελειώσει η μετατροπή
    sbrs r24, 0                     ; της θερμοκρασίας (r24=1), αλλιώς
    rjmp check_finished             ; περιμένουμε.

    rcall one_wire_reset            ; Αρχικοποιούμε και πάλι τη συσκευή, γιατί
                                    ; μετά τη μέτρηση επανέρχεται σε κατάσταση
                                    ; χαμηλής κατανάλωσης ισχύος.
    sbrs r24, 0                     ; Αν αποσυνδέθηκε επιστρέφουμε με τιμή 8000.
    rjmp no_device

    ldi r24, 0xCC                   ; Αγνοούμε τον έλεγχο για πολλές συσκευές.
    rcall one_wire_transmit_byte
    ldi r24, 0xBE                   ; Ζητάμε να γίνει ανάγνωση.
    rcall one_wire_transmit_byte


    rcall one_wire_receive_byte     ; Διαβάζουμε τα 2 bytes της θερμοκρασίας.
    push r24
    rcall one_wire_receive_byte
    mov r25,r24
    pop r24                         ; Στο τέλος έχουμε r25:r24 = high:low

    sbrs r25,0
    rjmp done
    dec r24                         ; Μετατροπή από συμπλήρωμα ως προς 2 σε
                                    ; συμπλήρωμα ως προς 1.
done:
    ser temp
    out DDRB, temp
	asr r24
    out PORTB, r24                  ; Εκτύπωση αποτελέσματος στην PORTB.
    ret                             ; Επιστροφή της θερμοκρασίας στους r25:r24.

no_device:                          ; Επιστροφή με r25:r24 = 8000 σε περίπτωση
    ldi r25, high(0x8000)             ; σφάλματος.
    ldi r24, low(0x8000)
    ret


; ###########################################################################
; one wire
; ###########################################################################

; This file includes routines implementing the one wire protocol over the PA4 pin of the microcontroller.
; Dependencies: wait.asm

; Routine: one_wire_receive_byte
; Description:
; This routine generates the necessary read
; time slots to receives a byte from the wire.
; return value: the received byte is returned in r24.
; registers affected: r27:r26 ,r25:r24
; routines called: one_wire_receive_bit
one_wire_receive_byte:
    ldi r27 ,8
    clr r26
loop_:
    rcall one_wire_receive_bit
    lsr r26
    sbrc r24 ,0
    ldi r24 ,0x80
    or r26 ,r24
    dec r27
    brne loop_
    mov r24 ,r26
    ret

; Routine: one_wire_receive_bit
; Description:
; This routine generates a read time slot across the wire.
; return value: The bit read is stored in the lsb of r24.
; if 0 is read or 1 if 1 is read.
; registers affected: r25:r24
; routines called: wait_usec
one_wire_receive_bit:
    sbi DDRA ,PA4
    cbi PORTA ,PA4 ; generate time slot
    ldi r24 ,0x02
    ldi r25 ,0x00
    rcall wait_usec
    cbi DDRA ,PA4 ; release the line
    cbi PORTA ,PA4
    ldi r24 ,10 ; wait 10 μs
    ldi r25 ,0
    rcall wait_usec
    clr r24 ; sample the line
    sbic PINA ,PA4
    ldi r24 ,1
    push r24
    ldi r24 ,49 ; delay 49 μs to meet the standards
    ldi r25 ,0 ; for a minimum of 60 μsec time slot
    rcall wait_usec ; and a minimum of 1 μsec recovery time
    pop r24
    ret

; Routine: one_wire_transmit_byte
; Description:
; This routine transmits a byte across the wire.
; parameters:
; r24: the byte to be transmitted must be stored here.
; return value: None.
; registers affected: r27:r26 ,r25:r24
; routines called: one_wire_transmit_bit
one_wire_transmit_byte:
    mov r26 ,r24
    ldi r27 ,8
_one_more_:
    clr r24
    sbrc r26 ,0
    ldi r24 ,0x01
    rcall one_wire_transmit_bit
    lsr r26
    dec r27
    brne _one_more_
    ret

; Routine: one_wire_transmit_bit
; Description:
; This routine transmits a bit across the wire.
; parameters:
; r24: if we want to transmit 1
; then r24 should be 1, else r24 should
; be cleared to transmit 0.
; return value: None.
; registers affected: r25:r24
; routines called: wait_usec
one_wire_transmit_bit:
    push r24 ; save r24
    sbi DDRA ,PA4
    cbi PORTA ,PA4 ; generate time slot
    ldi r24 ,0x02
    ldi r25 ,0x00
    rcall wait_usec
    pop r24 ; output bit
    sbrc r24 ,0
    sbi PORTA ,PA4
    sbrs r24 ,0
    cbi PORTA ,PA4
    ldi r24 ,58 ; wait 58 μsec for the
    ldi r25 ,0 ; device to sample the line
    rcall wait_usec
    cbi DDRA ,PA4 ; recovery time
    cbi PORTA ,PA4
    ldi r24 ,0x01
    ldi r25 ,0x00
    rcall wait_usec
    ret

; Routine: one_wire_reset
; Description:
; This routine transmits a reset pulse across the wire
; and detects any connected devices.
; parameters: None.
; return value: 1 is stored in r24
; if a device is detected, or 0 else.
; registers affected r25:r24
; routines called: wait_usec
one_wire_reset:
    sbi DDRA ,PA4 ; PA4 configured for output
    cbi PORTA ,PA4 ; 480 μsec reset pulse
    ldi r24 ,low(480)
    ldi r25 ,high(480)
    rcall wait_usec
    cbi DDRA ,PA4 ; PA4 configured for input
    cbi PORTA ,PA4
    ldi r24 ,100 ; wait 100 μsec for devices
    ldi r25 ,0 ; to transmit the presence pulse
    rcall wait_usec
    in r24 ,PINA ; sample the line
    push r24
    ldi r24 ,low(380) ; wait for 380 μsec
    ldi r25 ,high(380)
    rcall wait_usec
    pop r25 ; return 0 if no device was
    clr r24 ; detected or 1 else
    sbrs r25 ,PA4
    ldi r24 ,0x01
    ret

; ################################################################
;; Προκαλεί καθυστέρηση r25:r24 msec.
wait_msec:
	push r24				; 2 κύκλοι (0.250 μsec)
	push r25				; 2 κύκλοι
	ldi r24,  low(998)		; φόρτωσε τον καταχ. r25:r24 με 998 (1 κύκλος - 0.125 μsec)
	ldi r25,  high(998)		; 1 κύκλος (0.125 μsec)
	rcall wait_usec			; 3 κύκλοι (0.375 μsec), προκαλεί συνολικά καθυστέρηση 998.375 μsec
	pop r25					; 2 κύκλοι (0.250 μsec)
	pop r24					; 2 κύκλοι
	sbiw r24,  1			; 2 κύκλοι
	brne wait_msec			; 1 ή 2 κύκλοι (0.125 ή 0.250 μsec)
	ret						; 4 κύκλοι (0.500 μsec)

;; Προκαλεί καθυστέρηση r25:r24 μsec.
wait_usec:
	sbiw r24, 1				; 2 κύκλοι (0.250 μsec)
	nop						; 1 κύκλος (0.125 μsec)
	nop						; 1 κύκλος (0.125 μsec)
	nop						; 1 κύκλος (0.125 μsec)
	nop						; 1 κύκλος (0.125 μsec)
	brne wait_usec			; 1 ή 2 κύκλοι (0.125 ή 0.250 μsec)
	ret						; 4 κύκλοι (0.500 μsec)

; #################################################################
; screen
; #################################################################
write_2_nibbles:
	push r24				; στέλνει τα 4 MSB
	in r25, PIND			; διαβάζονται τα 4 LSB και τα ξαναστέλνουμε
	andi r25, 0x0f			; για να μην χαλάσουμε την όποια προηγούμενη κατάσταση
	andi r24, 0xf0			; απομονώνονται τα 4 MSB και
	add r24, r25			; συνδυάζονται με τα προϋπάρχοντα 4 LSB
	out PORTD, r24			; και δίνονται στην έξοδο
	sbi PORTD, PD3			; δημιουργείται παλμός Εnable στον ακροδέκτη PD3
	cbi PORTD, PD3			; PD3=1 και μετά PD3=0
	pop r24					; στέλνει τα 4 LSB. Ανακτάται το byte.
	swap r24				; εναλλάσσονται τα 4 MSB με τα 4 LSB
	andi r24, 0xf0			; που με την σειρά τους αποστέλλονται
	add r24, r25
	out PORTD, r24
	sbi PORTD, PD3			; Νέος παλμός Εnable
	cbi PORTD, PD3
	ret

lcd_data:
	sbi PORTD, PD2			; επιλογή του καταχωρήτη δεδομένων (PD2=1)
	rcall write_2_nibbles	; αποστολή του byte
	ldi r24, 43				; αναμονή 43μsec μέχρι να ολοκληρωθεί η λήψη
	ldi r25, 0				; των δεδομένων από τον ελεγκτή της lcd
	rcall wait_usec
	ret

lcd_command:
	cbi PORTD, PD2			; επιλογή του καταχωρητή εντολών (PD2=0)
	rcall write_2_nibbles	; αποστολή της εντολής και αναμονή 39μsec
	ldi r24, 39				; για την ολοκλήρωση της εκτέλεσης της από τον ελεγκτή της lcd.
	ldi r25, 0				; ΣΗΜ.: υπάρχουν δύο εντολές, οι clear display και return home,
	rcall wait_usec			; που απαιτούν σημαντικά μεγαλύτερο χρονικό διάστημα.
	ret

lcd_init:
	ldi r24, 40				; Όταν ο ελεγκτής της lcd τροφοδοτείται με
	ldi r25, 0				; ρεύμα εκτελεί την δική του αρχικοποίηση.
	rcall wait_msec			; Αναμονή 40 msec μέχρι αυτή να ολοκληρωθεί.
	ldi r24, 0x30			; εντολή μετάβασης σε 8 bit mode
	out PORTD, r24			; επειδή δεν μπορούμε να είμαστε βέβαιοι
	sbi PORTD, PD3			; για τη διαμόρφωση εισόδου του ελεγκτή
	cbi PORTD, PD3			; της οθόνης, η εντολή αποστέλλεται δύο φορές
	ldi r24, 39
	ldi r25, 0				; εάν ο ελεγκτής της οθόνης βρίσκεται σε 8-bit mode
	rcall wait_usec			; δεν θα συμβεί τίποτα, αλλά αν ο ελεγκτής έχει διαμόρφωση
							; εισόδου 4 bit θα μεταβεί σε διαμόρφωση 8 bit
	ldi r24, 0x30
	out PORTD, r24
	sbi PORTD, PD3
	cbi PORTD, PD3
	ldi r24, 39
	ldi r25, 0
	rcall wait_usec
	ldi r24, 0x20			; αλλαγή σε 4-bit mode
	out PORTD, r24
	sbi PORTD, PD3
	cbi PORTD, PD3
	ldi r24, 39
	ldi r25, 0
	rcall wait_usec
	ldi r24, 0x28			; επιλογή χαρακτήρων μεγέθους 5x8 κουκίδων
	rcall lcd_command		; και εμφάνιση δύο γραμμών στην οθόνη
	ldi r24, 0x0e			; ενεργοποίηση της οθόνης, εμφάνιση του κέρσορα
	rcall lcd_command
	ldi r24, 0x01			; καθαρισμός της οθόνης
	rcall lcd_command
	ldi r24, low(1530)
	ldi r25, high(1530)
	rcall wait_usec
	ldi r24, 0x06			; ενεργοποίηση αυτόματης αύξησης κατά 1 της διεύθυνσης
	rcall lcd_command		; που είναι αποθηκευμένη στον μετρητή διευθύνσεων και
							; απενεργοποίηση της ολίσθησης ολόκληρης της οθόνης
	ret
