---
mainfont: Open Sans
documentclass: extarticle
fontsize: 12pt
geometry: margin=1in
papersize: a4
title: "4η Σειρά ασκήσεων, Εργαστήριο Μικροϋπολογιστών"
author:
- "Αγιάννης Κωνσταντίνος 03116352"
- "Τσιαχρήστος Δημήτριος 03116048"
---

# Ζήτημα 4.1 

### Κώδικας σε asm

Με την one_wire_reset ελέγχουμε την ύπαρξη συσκευής. Η σειρά ροής των εντολών διασύνδεσης είναι  βάσει της 
εκφώνησης.Για μετατροπή σε βαθμούς κελσίου στη διαίρεση με το 2 κάνουμε asr και όχι lsr για διατήρηση προσήμου.
Έξοδος στην PORTB.Στην περίπτωση μη ανίχνευσης συσκευής φορτώνουμε στους r25,r24 το 0x8000 μέσω της no_device.

```asm
.include "m16def.inc"
.def temp = r18

    ;Δημιουργία της Στοίβας.
    ldi r24,LOW(RAMEND)
    out SPL,r24
    ldi r25,HIGH(RAMEND)
    out SPH,r25

start:
    rcall read_temp
    ldi r24, low(1000)
    ldi r25, high(1000)
    rcall wait_msec
    rjmp start

; Ρουτίνα που επιστρέφει την μετρούμενη θερμοκρασία στο ζεύγος r25:r24
; σε δυαδική μορφή συμπληρώματος ως προς 1. Αν εμφανιστεί οποιοδήποτε σφάλμα,
; επιστρέφουμε την τιμή 8000.
read_temp:
    rcall one_wire_reset            ; Αρχικοποίηση συσκευής.
    sbrs r24, 0                     ; Αν δε βρεθεί συσκευή (r24=0),
    rjmp no_device                  ; επιστρέφουμε με τιμή 8000.

    ldi r24, 0xCC                   ; Αγνοούμε τον έλεγχο για πολλές συσκευές.
    rcall one_wire_transmit_byte
    ldi r24, 0x44                   ; Ζητάμε να ξεκινήσει η μέτρηση της
    rcall one_wire_transmit_byte    ; θερμοκρασίας.

check_finished:
    rcall one_wire_receive_bit      ; Ελέγχουμε αν έχει τελειώσει η μετατροπή
    sbrs r24````````, 0                     ; της θερμοκρασίας (r24=1), αλλιώς
    rjmp check_finished             ; περιμένουμε.

    rcall one_wire_reset            ; Αρχικοποιούμε και πάλι τη συσκευή, γιατί
                                    ; μετά τη μέτρηση επανέρχεται σε κατάσταση
                                    ; χαμηλής κατανάλωσης ισχύος.
    sbrs r24, 0                     ; Αν αποσυνδέθηκε επιστρέφουμε με τιμή 8000.
    rjmp no_device

    ldi r24, 0xCC                   ; Αγνοούμε τον έλεγχο για πολλές συσκευές.
    rcall one_wire_transmit_byte
    ldi r24, 0xBE                   ; Ζητάμε να γίνει ανάγνωση.
    rcall one_wire_transmit_byte


    rcall one_wire_receive_byte     ; Διαβάζουμε τα 2 bytes της θερμοκρασίας.
    push r24
    rcall one_wire_receive_byte
    mov r25,r24
    pop r24                         ; Στο τέλος έχουμε r25:r24 = high:low

    sbrs r25,0
    rjmp done
    dec r24                         ; Μετατροπή από συμπλήρωμα ως προς 2 σε
                                    ; συμπλήρωμα ως προς 1.
done:
    ser temp
    out DDRB, temp                  ; Έξοδος στην Β
	asr r24                         ; arithmetic right shift για να κρατάμε και το πρόσημο  διαιρώντας και με το 2
    out PORTB, r24                  ; Εκτύπωση αποτελέσματος στην PORTB.
    ret                             ; Επιστροφή της θερμοκρασίας στους r25:r24.

no_device:                          ; Επιστροφή με r25:r24 = 8000 σε περίπτωση
    ldi r25, high(0x8000)             ; σφάλματος.
    ldi r24, low(0x8000)
    ret


; ###########################################################################
; one wire
; ###########################################################################

; This file includes routines implementing the one wire protocol over the PA4 pin of the microcontroller.
; Dependencies: wait.asm

; Routine: one_wire_receive_byte
; Description:
; This routine generates the necessary read
; time slots to receives a byte from the wire.
; return value: the received byte is returned in r24.
; registers affected: r27:r26 ,r25:r24
; routines called: one_wire_receive_bit
one_wire_receive_byte:
    ldi r27 ,8
    clr r26
loop_:
    rcall one_wire_receive_bit
    lsr r26
    sbrc r24 ,0
    ldi r24 ,0x80
    or r26 ,r24
    dec r27
    brne loop_
    mov r24 ,r26
    ret

; Routine: one_wire_receive_bit
; Description:
; This routine generates a read time slot across the wire.
; return value: The bit read is stored in the lsb of r24.
; if 0 is read or 1 if 1 is read.
; registers affected: r25:r24
; routines called: wait_usec
one_wire_receive_bit:
    sbi DDRA ,PA4
    cbi PORTA ,PA4 ; generate time slot
    ldi r24 ,0x02
    ldi r25 ,0x00
    rcall wait_usec
    cbi DDRA ,PA4 ; release the line
    cbi PORTA ,PA4
    ldi r24 ,10 ; wait 10 μs
    ldi r25 ,0
    rcall wait_usec
    clr r24 ; sample the line
    sbic PINA ,PA4
    ldi r24 ,1
    push r24
    ldi r24 ,49 ; delay 49 μs to meet the standards
    ldi r25 ,0 ; for a minimum of 60 μsec time slot
    rcall wait_usec ; and a minimum of 1 μsec recovery time
    pop r24
    ret

; Routine: one_wire_transmit_byte
; Description:
; This routine transmits a byte across the wire.
; parameters:
; r24: the byte to be transmitted must be stored here.
; return value: None.
; registers affected: r27:r26 ,r25:r24
; routines called: one_wire_transmit_bit
one_wire_transmit_byte:
    mov r26 ,r24
    ldi r27 ,8
_one_more_:
    clr r24
    sbrc r26 ,0
    ldi r24 ,0x01
    rcall one_wire_transmit_bit
    lsr r26
    dec r27
    brne _one_more_
    ret

; Routine: one_wire_transmit_bit
; Description:
; This routine transmits a bit across the wire.
; parameters:
; r24: if we want to transmit 1
; then r24 should be 1, else r24 should
; be cleared to transmit 0.
; return value: None.
; registers affected: r25:r24
; routines called: wait_usec
one_wire_transmit_bit:
    push r24 ; save r24
    sbi DDRA ,PA4
    cbi PORTA ,PA4 ; generate time slot
    ldi r24 ,0x02
    ldi r25 ,0x00
    rcall wait_usec
    pop r24 ; output bit
    sbrc r24 ,0
    sbi PORTA ,PA4
    sbrs r24 ,0
    cbi PORTA ,PA4
    ldi r24 ,58 ; wait 58 μsec for the
    ldi r25 ,0 ; device to sample the line
    rcall wait_usec
    cbi DDRA ,PA4 ; recovery time
    cbi PORTA ,PA4
    ldi r24 ,0x01
    ldi r25 ,0x00
    rcall wait_usec
    ret

; Routine: one_wire_reset
; Description:
; This routine transmits a reset pulse across the wire
; and detects any connected devices.
; parameters: None.
; return value: 1 is stored in r24
; if a device is detected, or 0 else.
; registers affected r25:r24
; routines called: wait_usec
one_wire_reset:
    sbi DDRA ,PA4 ; PA4 configured for output
    cbi PORTA ,PA4 ; 480 μsec reset pulse
    ldi r24 ,low(480)
    ldi r25 ,high(480)
    rcall wait_usec
    cbi DDRA ,PA4 ; PA4 configured for input
    cbi PORTA ,PA4
    ldi r24 ,100 ; wait 100 μsec for devices
    ldi r25 ,0 ; to transmit the presence pulse
    rcall wait_usec
    in r24 ,PINA ; sample the line
    push r24
    ldi r24 ,low(380) ; wait for 380 μsec
    ldi r25 ,high(380)
    rcall wait_usec
    pop r25 ; return 0 if no device was
    clr r24 ; detected or 1 else
    sbrs r25 ,PA4
    ldi r24 ,0x01
    ret

; ################################################################
;; Προκαλεί καθυστέρηση r25:r24 msec.
wait_msec:
	push r24				; 2 κύκλοι (0.250 μsec)
	push r25				; 2 κύκλοι
	ldi r24,  low(998)		; φόρτωσε τον καταχ. r25:r24 με 998 (1 κύκλος - 0.125 μsec)
	ldi r25,  high(998)		; 1 κύκλος (0.125 μsec)
	rcall wait_usec			; 3 κύκλοι (0.375 μsec), προκαλεί συνολικά καθυστέρηση 998.375 μsec
	pop r25					; 2 κύκλοι (0.250 μsec)
	pop r24					; 2 κύκλοι
	sbiw r24,  1			; 2 κύκλοι
	brne wait_msec			; 1 ή 2 κύκλοι (0.125 ή 0.250 μsec)
	ret						; 4 κύκλοι (0.500 μsec)

;; Προκαλεί καθυστέρηση r25:r24 μsec.
wait_usec:
	sbiw r24, 1				; 2 κύκλοι (0.250 μsec)
	nop						; 1 κύκλος (0.125 μsec)
	nop						; 1 κύκλος (0.125 μsec)
	nop						; 1 κύκλος (0.125 μsec)
	nop						; 1 κύκλος (0.125 μsec)
	brne wait_usec			; 1 ή 2 κύκλοι (0.125 ή 0.250 μsec)
	ret						; 4 κύκλοι (0.500 μsec)

; #################################################################
; screen
; #################################################################
write_2_nibbles:
	push r24				; στέλνει τα 4 MSB
	in r25, PIND			; διαβάζονται τα 4 LSB και τα ξαναστέλνουμε
	andi r25, 0x0f			; για να μην χαλάσουμε την όποια προηγούμενη κατάσταση
	andi r24, 0xf0			; απομονώνονται τα 4 MSB και
	add r24, r25			; συνδυάζονται με τα προϋπάρχοντα 4 LSB
	out PORTD, r24			; και δίνονται στην έξοδο
	sbi PORTD, PD3			; δημιουργείται παλμός Εnable στον ακροδέκτη PD3
	cbi PORTD, PD3			; PD3=1 και μετά PD3=0
	pop r24					; στέλνει τα 4 LSB. Ανακτάται το byte.
	swap r24				; εναλλάσσονται τα 4 MSB με τα 4 LSB
	andi r24, 0xf0			; που με την σειρά τους αποστέλλονται
	add r24, r25
	out PORTD, r24
	sbi PORTD, PD3			; Νέος παλμός Εnable
	cbi PORTD, PD3
	ret

lcd_data:
	sbi PORTD, PD2			; επιλογή του καταχωρήτη δεδομένων (PD2=1)
	rcall write_2_nibbles	; αποστολή του byte
	ldi r24, 43				; αναμονή 43μsec μέχρι να ολοκληρωθεί η λήψη
	ldi r25, 0				; των δεδομένων από τον ελεγκτή της lcd
	rcall wait_usec
	ret

lcd_command:
	cbi PORTD, PD2			; επιλογή του καταχωρητή εντολών (PD2=0)
	rcall write_2_nibbles	; αποστολή της εντολής και αναμονή 39μsec
	ldi r24, 39				; για την ολοκλήρωση της εκτέλεσης της από τον ελεγκτή της lcd.
	ldi r25, 0				; ΣΗΜ.: υπάρχουν δύο εντολές, οι clear display και return home,
	rcall wait_usec			; που απαιτούν σημαντικά μεγαλύτερο χρονικό διάστημα.
	ret

lcd_init:
	ldi r24, 40				; Όταν ο ελεγκτής της lcd τροφοδοτείται με
	ldi r25, 0				; ρεύμα εκτελεί την δική του αρχικοποίηση.
	rcall wait_msec			; Αναμονή 40 msec μέχρι αυτή να ολοκληρωθεί.
	ldi r24, 0x30			; εντολή μετάβασης σε 8 bit mode
	out PORTD, r24			; επειδή δεν μπορούμε να είμαστε βέβαιοι
	sbi PORTD, PD3			; για τη διαμόρφωση εισόδου του ελεγκτή
	cbi PORTD, PD3			; της οθόνης, η εντολή αποστέλλεται δύο φορές
	ldi r24, 39
	ldi r25, 0				; εάν ο ελεγκτής της οθόνης βρίσκεται σε 8-bit mode
	rcall wait_usec			; δεν θα συμβεί τίποτα, αλλά αν ο ελεγκτής έχει διαμόρφωση
							; εισόδου 4 bit θα μεταβεί σε διαμόρφωση 8 bit
	ldi r24, 0x30
	out PORTD, r24
	sbi PORTD, PD3
	cbi PORTD, PD3
	ldi r24, 39
	ldi r25, 0
	rcall wait_usec
	ldi r24, 0x20			; αλλαγή σε 4-bit mode
	out PORTD, r24
	sbi PORTD, PD3
	cbi PORTD, PD3
	ldi r24, 39
	ldi r25, 0
	rcall wait_usec
	ldi r24, 0x28			; επιλογή χαρακτήρων μεγέθους 5x8 κουκίδων
	rcall lcd_command		; και εμφάνιση δύο γραμμών στην οθόνη
	ldi r24, 0x0e			; ενεργοποίηση της οθόνης, εμφάνιση του κέρσορα
	rcall lcd_command
	ldi r24, 0x01			; καθαρισμός της οθόνης
	rcall lcd_command
	ldi r24, low(1530)
	ldi r25, high(1530)
	rcall wait_usec
	ldi r24, 0x06			; ενεργοποίηση αυτόματης αύξησης κατά 1 της διεύθυνσης
	rcall lcd_command		; που είναι αποθηκευμένη στον μετρητή διευθύνσεων και
							; απενεργοποίηση της ολίσθησης ολόκληρης της οθόνης
	ret

```



### Κώδικας σε c

Οι υπορουτίνες διασύνδεσης με τον αισθητήρα είναι οι ακριβώς αντίστοιχες ανά εντολή με τις δοσμένες αντίστοιχες σε
assembly. Ροή εντολών διασύνδεσης όπως στην assembly. Τα bytes που στέλνονται σε μορφή συμπληρώματος ως προς 2 
τα μετατρέπουμε σε βαθμούς κελσίου με την comp2_to_celcius. Αποτέλεσμα στην PORTB σε hex μορφή. Στην περίπτωση 
μη ανίχνευσης συσκευής στέλνεται το 0x8000.


```c
#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdint.h>
//#define KEYBOARD

#define _NOP() do { __asm__ __volatile__ ("nop"); } while (0) //ορισμός καθυστέρησης

char t[16] = {1,2,3,0xA,4,5,6,0xB,7,8,9,0xC,0xE,0,0xF,0xD};
char to_ascii[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

//metatropi apo sympliroma 2 se kelsiou
int comp2_to_celcius(uint8_t msbs,uint8_t lsbs)
{
   int16_t celcius = (msbs << 8) + lsbs;
   return celcius / 2;
    
}

int one_wire_reset()
{
	DDRA |= (1 << 4);
	PORTA &= ~(1<<4);
	_delay_us(480);
	DDRA &= ~(1 << 4);
	PORTA &= ~(1<<4);
	_delay_us(100);
	char PA4_digit = PINA & 0x10;  //care only about PA4
	_delay_us(380);
	if(PA4_digit) return 0;
	else return 1;
}

int one_wire_receive_bit()
{
	DDRA |= (1 << 4);
	PORTA &~(1<<4);
	_delay_us(2);
	DDRA &= ~(1 << 4);
	PORTA &= ~(1<<4);
	_delay_us(10);
	char bit = (PINA >> 4) & 1;
 
	_delay_us(49);
	return bit;
 }
 
 int one_wire_receive_byte()
 {
	unsigned int res = 0;
	for(int i = 0;i < 8;i++)
		res = (res >> 1) | (one_wire_receive_bit() << 7);
	return res;
}
 
 void one_wire_transmit_bit(char x)
 {
	DDRA |= (1 << 4);
	PORTA &= ~(1<<4);
	_delay_us(2);
	x = x & 1;
	if(x) PORTA |= (1<<4);
	else PORTA &= ~(1<<4);
	_delay_us(58);
	DDRA &= ~(1 << DDA4);
	PORTA &= ~(1<<4);
	_delay_us(1);
}
 
void one_wire_transmit_byte(char x)
{
	for(int i=0;i<8;i++){
		one_wire_transmit_bit(x & 1);
		x >>= 1;
	}
}



//****************************************************************************

void init_keyboard()
{
    DDRC = 0xf0;
}


int msb(int x)
{
    int res = 0;
    while(x > 1){
        res++;
        x>>=1;
    }
    return res;
}

//only one key pressed at a time
//if nothing pressed: return -1
int scan_keypad()
{
    int key;
    for(int i = 4;i < 8;i++){
        PORTC = 1<<i;
        _NOP();
		_NOP();
        key = PINC & 0xf;
        if(key != 0)
            return 4*(i-4)+msb(key);
    }
    return -1;
}
int prev = -1;

int read_spark(){
	int t1,t2;
	do{
		t1 = scan_keypad();
		_delay_ms (16);
		t2 = scan_keypad();
	}while(t1 != t2);
	return t2;
}

int scan_rising_edge()
{
    do{
        int tmp;
        while((tmp = read_spark()) == prev)
            ;
        prev = tmp;
    } while(prev == -1);
	return prev;
}

void pd3_pulse()
{
    char temp = PORTD;
    PORTD = temp | (1<<3);
    PORTD = temp;
}

void write_2_nibbles(char x)
{
    char lsb4 = PORTD & 0x0f;
    PORTD = lsb4 | (x & 0xf0);
    pd3_pulse();
    PORTD = lsb4 | (x << 4);
    pd3_pulse();
}

void lcd_data(char x)
{
    PORTD = PORTD | (1<<2);
    write_2_nibbles(x);
    _delay_us(43);
}

void lcd_command(char x)
{
    PORTD = PORTD & (~(1<<2));
    write_2_nibbles(x);
    _delay_us(39);
}

void lcd_init()
{
    DDRD = 0xff;

    _delay_ms(40);
    PORTD = 0x30;
    pd3_pulse();
    _delay_us(39);
    
    PORTD = 0x30;
    pd3_pulse();
    _delay_us(39);
    
    PORTD = 0x20;
    pd3_pulse();
    _delay_us(39);
    
    lcd_command(0x28);
    lcd_command(0x0c);
    lcd_command(0x01);
    
    _delay_us(1530);
    lcd_command(0x06);
    
}

//****************************************************************
//****************************************************************

int read_temperature()
{
#ifdef KEYBOARD                          //an theloume eisagogi apo keypad 
	int k1 = t[scan_rising_edge()];      //eisagogi twn arithmwn basei tou  char t[16] parapanw 
	int k2 = t[scan_rising_edge()];
	int k3 = t[scan_rising_edge()];
	int k4 = t[scan_rising_edge()];
	int tmp = ((k1 << 12) | (k2 << 8) | (k3 << 4) | k4);     //ftiaxnei ton arithmo
	if(tmp != 0x8000)
		return tmp/2;
	else
		return 0x8000;
	
#else
    int temperature = 0x8000;    //gia mi yparxi syskeuis
	
	
	int detect = one_wire_reset();     //elegxos gia yparxi syskeuis
	if(!detect) return 0x8000;         //an oxi steile 0x8000
	one_wire_transmit_byte(0xCC);        //an yparxi steile tin entoli 0xCC
	one_wire_transmit_byte(0x44);        //meta tin 0x44
	
	while(one_wire_receive_bit());      //perimene mexri na ypologistei h thermokrasia,otan ginei ayto tha stalthei
	//lcd_data('1');                    //apo ti syskeui bit=1 pou to pairnoume me tin one_wire_receive_bit()
	detect = one_wire_reset();          //xana elegxos gia yparxi syskeuis
	if(detect){
		one_wire_transmit_byte(0xCC);      //an yparxei steile tin 0xCC
		one_wire_transmit_byte(0xBE);      //steile tin 0xBE
		uint8_t lsbits = one_wire_receive_byte();  //pairnoume  ta bytes  pou stelnontai
		uint8_t msbits = one_wire_receive_byte();
		temperature = comp2_to_celcius(msbits,lsbits);    //metatropi apo sympliroma tou 2 se bathmous kelsiou
	}
	return temperature;
	
#endif
}


int main()
{
	DDRB = 0xff; //output
	while(1){
		int temperature = read_temperature();
		PORTB = temperature & 0xff;    //emfanisi thermokrasias sthn B σε hex
	}
}


```

# Ζήτημα 4.2

Όλες οι υπορουτίνες για διάβασμα από πληκτρολόγιο και εμφάνιση στην οθόνη είναι από προηγούμενες σειρές. Οι 
scan_key, keypad_to_hex είναι για είσοδο από keypad. Μέσω της keypad_hex στον r28 επιστρέφουμε και τη hex μορφή 
κάθε χαρακτήρα. Στην περίπτωση μη ανίχνευσης συσκευής ή όταν πληκτρολογούμε 0x8000 μέσω της print_error 
εμφανίζεται στην οθόνη NO Device.Αν ανιχνευθεί όντως συσκευή ή δεν πατήσαμε 0x8000 στο detected (και κάτω )
εμφανίζεται ο αριθμός στην οθόνη.

### Κώδικας σε asm

```asm
.include "m16def.inc"
.DSEG
	_tmp_: .byte 2

.CSEG
//#define KEYBOARD

.def temp = r18
.def first = r19

    ; Αρχικοποίηση μεταβλητής _tmp_ .
    ldi temp, 0
    ldi r26, low(_tmp_)
    ldi r27, high(_tmp_)
    st X+, temp
    st X, temp

    ; Αρχικοποίηση της PORTC για το πληκτρολόγιο.
    ldi temp, 0xf0
    out DDRC, temp

    ;Δημιουργία της Στοίβας.
    ldi r24,LOW(RAMEND)
    out SPL,r24
    ldi r25,HIGH(RAMEND)
    out SPH,r25

    ; Αρχικοποίηση της PORTC για το πληκτρολόγιο.
	ldi temp, 0xf0
	out DDRC, temp

    ;Αρχικοποίηση της PORTD για την οθόνη LCD.
	ldi temp, 0xff
	out DDRD, temp

    ;Αρχικοποίηση οθόνης.
	rcall lcd_init

#ifdef KEYBOARD
start:
    ldi r20,0
    ldi r21,0
                             
                             ;eisagoyme apo pliktrologio kai ftiaxnoume katallila ton arithmo(swap,or)

	rcall scan_key
	rcall keypad_to_hex
	mov r21,r28           ;metafora ths hex morfis tou xaraktira
	swap r21               ;enallagi msb-lsb
	rcall scan_key
	rcall keypad_to_hex
	or r21, r28              ;or me to epomeno
	rcall scan_key
	rcall keypad_to_hex
	mov r20, r28
	swap r20
	rcall scan_key
	rcall keypad_to_hex
	or r20, r28
#if 0
	ser temp
    out DDRB, temp
	//asr r20
    out PORTB, r20       ;exodos sthn B
#endif
	mov r24,r20
	mov r25,r21
    rcall print_temp
;    ldi r24, low(1000)
;    ldi r25, high(1000)
;    rcall wait_msec        ;kathisterisi
    rjmp start
#else
start:
	

    rcall read_temp
	
	rcall print_temp
    ldi r24, low(1000)
    ldi r25, high(1000)
    rcall wait_msec
    rjmp start
#endif

#define tmp r19

scan_key:
	ldi r24,20; ms για σπινθιρισμό
	rcall scan_keypad_rising_edge
	clr tmp
	or tmp,r24
	or tmp,r25
	cpi tmp,0
	breq scan_key
	ret	


read_temp:
    rcall one_wire_reset            ; Αρχικοποίηση συσκευής.
    sbrs r24, 0                     ; Αν δε βρεθεί συσκευή (r24=0),
    rjmp no_device                  ; επιστρέφουμε με τιμή 8000.

    ldi r24, 0xCC                   ; Αγνοούμε τον έλεγχο για πολλές συσκευές.
    rcall one_wire_transmit_byte
    ldi r24, 0x44                   ; Ζητάμε να ξεκινήσει η μέτρηση της
    rcall one_wire_transmit_byte    ; θερμοκρασίας.

check_finished:
    rcall one_wire_receive_bit      ; Ελέγχουμε αν έχει τελειώσει η μετατροπή
    sbrs r24, 0                     ; της θερμοκρασίας (r24=1), αλλιώς
    rjmp check_finished             ; περιμένουμε.

    rcall one_wire_reset            ; Αρχικοποιούμε και πάλι τη συσκευή, γιατί
                                    ; μετά τη μέτρηση επανέρχεται σε κατάσταση
                                    ; χαμηλής κατανάλωσης ισχύος.
    sbrs r24, 0                     ; Αν αποσυνδέθηκε επιστρέφουμε με τιμή 8000.
    rjmp no_device

    ldi r24, 0xCC                   ; Αγνοούμε τον έλεγχο για πολλές συσκευές.
    rcall one_wire_transmit_byte
    ldi r24, 0xBE                   ; Ζητάμε να γίνει ανάγνωση.
    rcall one_wire_transmit_byte


    rcall one_wire_receive_byte     ; Διαβάζουμε τα 2 bytes της θερμοκρασίας.
    push r24
    rcall one_wire_receive_byte
    mov r25, r24                    ; Στο τέλος έχουμε r25:r24 = high:low
    pop r24

    sbrs r25,0
    rjmp done
done:
    ser temp
    out DDRB, temp
	clc
	ror r25 ; put lsb to carry
	ror r24 ; get msb from carry
    out PORTB, r24                  ; Εκτύπωση αποτελέσματος στην PORTA.
    ret                             ; Επιστροφή της θερμοκρασίας στους r25:r24.

no_device:                          ; Επιστροφή με r25:r24 = 8000 σε περίπτωση
    ldi r25, high(0x8000)             ; σφάλματος.
    ldi r24, low(0x8000)
    ret


print_error:
    ldi r24, 0x01			        ; Καθαρισμός της οθόνης.
    rcall lcd_command
    ldi r24, low(1530)
    ldi r25, high(1530)
    rcall wait_usec
    ldi r24, 'N'			         ; Τύπωμα "NO Device".
    rcall lcd_data
    ldi r24, 'O'
    rcall lcd_data
    ldi r24, ' '
    rcall lcd_data
    ldi r24, 'D'
    rcall lcd_data
    ldi r24, 'e'
    rcall lcd_data
    ldi r24, 'v'
    rcall lcd_data
    ldi r24, 'i'
    rcall lcd_data
    ldi r24, 'c'
    rcall lcd_data
    ldi r24, 'e'
    rcall lcd_data
    ret
#if 1
print_temp:
	
	push r24
	push r25
	ldi r24, 0x01			        ; Καθαρισμός της οθόνης.
    rcall lcd_command
    ldi r24, low(1530)
    ldi r25, high(1530)
    rcall wait_usec       ;mikri kathisterisi
	pop r25
	pop r24

	cpi r24,0
	brne detected
	cpi r25,0x80; ????????????
	brne detected
	rcall print_error      ;an einai 0x80 emfanise NO Device
	ret
detected:              ;αν ανιχνεύθηκε ή πατήθηκε όχι 0x8000
	mov temp, r24
	cpi r24, 0
	brmi minus
   
    ldi r24, '+'    
     rcall lcd_data ; και εκτύπωση προσήμου
    rjmp next_print
minus:
	ldi r24, '-'
    rcall lcd_data
    neg temp                        ; τότε υπολογίζουμε το συμπλήρωμά του.

next_print:
#if 0
	
	ser tmp
    out DDRB, tmp
    out PORTB, r25   
#endif
    ldi first, 0
    ldi r24, 0

    cpi temp,100
    brlo decades
    ldi first, 1
    ldi r24, '1'       ;an temp >=100 emfanise ws msdigit to 1 sthn othoni alliws pigaine decades
    rcall lcd_data
    subi temp,100

    ldi r24,0
decades:
    cpi temp,10
    brlo print_dec    ;an <10 pigaine print_dec gia emfanisi digit
    inc r24
    subi temp,10      ;alliws afairese 10  (ayto mexri temp < 10)
    rjmp decades
print_dec:
    cpi r24,0
    sbrs first,0
    breq digit
    subi r24,-48                    ; ASCIIοποίηση δεκάδων.
    rcall lcd_data                   ;emfainisi sthn othoni
digit:
    mov r24, temp
    subi r24,-48                    ; ASCIIοποίηση μονάδων
    rcall lcd_data

    ldi r24, 0xb2                   ; Εκτύπωση degree
    rcall lcd_data
    ldi r24, 'C'                     ; κελσίου
    rcall lcd_data
    ret
#endif
; ###########################################################################
; one wire
; ###########################################################################

; Routine: one_wire_receive_byte
; Description:
; This routine generates the necessary read
; time slots to receives a byte from the wire.
; return value: the received byte is returned in r24.
; registers affected: r27:r26 ,r25:r24
; routines called: one_wire_receive_bit
one_wire_receive_byte:
    ldi r27 ,8
    clr r26
loop_:
    rcall one_wire_receive_bit
    lsr r26
    sbrc r24 ,0
    ldi r24 ,0x80
    or r26 ,r24
    dec r27
    brne loop_
    mov r24 ,r26
    ret

; Routine: one_wire_receive_bit
; Description:
; This routine generates a read time slot across the wire.
; return value: The bit read is stored in the lsb of r24.
; if 0 is read or 1 if 1 is read.
; registers affected: r25:r24
; routines called: wait_usec
one_wire_receive_bit:
    sbi DDRA ,PA4
    cbi PORTA ,PA4 ; generate time slot
    ldi r24 ,0x02
    ldi r25 ,0x00
    rcall wait_usec
    cbi DDRA ,PA4 ; release the line
    cbi PORTA ,PA4
    ldi r24 ,10 ; wait 10 μs
    ldi r25 ,0
    rcall wait_usec
    clr r24 ; sample the line
    sbic PINA ,PA4
    ldi r24 ,1
    push r24
    ldi r24 ,49 ; delay 49 μs to meet the standards
    ldi r25 ,0 ; for a minimum of 60 μsec time slot
    rcall wait_usec ; and a minimum of 1 μsec recovery time
    pop r24
    ret

; Routine: one_wire_transmit_byte
; Description:
; This routine transmits a byte across the wire.
; parameters:
; r24: the byte to be transmitted must be stored here.
; return value: None.
; registers affected: r27:r26 ,r25:r24
; routines called: one_wire_transmit_bit
one_wire_transmit_byte:
    mov r26 ,r24
    ldi r27 ,8
_one_more_:
    clr r24
    sbrc r26 ,0
    ldi r24 ,0x01
    rcall one_wire_transmit_bit
    lsr r26
    dec r27
    brne _one_more_
    ret

; Routine: one_wire_transmit_bit
; Description:
; This routine transmits a bit across the wire.
; parameters:
; r24: if we want to transmit 1
; then r24 should be 1, else r24 should
; be cleared to transmit 0.
; return value: None.
; registers affected: r25:r24
; routines called: wait_usec
one_wire_transmit_bit:
    push r24 ; save r24
    sbi DDRA ,PA4
    cbi PORTA ,PA4 ; generate time slot
    ldi r24 ,0x02
    ldi r25 ,0x00
    rcall wait_usec
    pop r24 ; output bit
    sbrc r24 ,0
    sbi PORTA ,PA4
    sbrs r24 ,0
    cbi PORTA ,PA4
    ldi r24 ,58 ; wait 58 μsec for the
    ldi r25 ,0 ; device to sample the line
    rcall wait_usec
    cbi DDRA ,PA4 ; recovery time
    cbi PORTA ,PA4
    ldi r24 ,0x01
    ldi r25 ,0x00
    rcall wait_usec
    ret

; Routine: one_wire_reset
; Description:
; This routine transmits a reset pulse across the wire
; and detects any connected devices.
; parameters: None.
; return value: 1 is stored in r24
; if a device is detected, or 0 else.
; registers affected r25:r24
; routines called: wait_usec
one_wire_reset:
    sbi DDRA ,PA4 ; PA4 configured for output
    cbi PORTA ,PA4 ; 480 μsec reset pulse
    ldi r24 ,low(480)
    ldi r25 ,high(480)
    rcall wait_usec
    cbi DDRA ,PA4 ; PA4 configured for input
    cbi PORTA ,PA4
    ldi r24 ,100 ; wait 100 μsec for devices
    ldi r25 ,0 ; to transmit the presence pulse
    rcall wait_usec
    in r24 ,PINA ; sample the line
    push r24
    ldi r24 ,low(380) ; wait for 380 μsec
    ldi r25 ,high(380)
    rcall wait_usec
    pop r25 ; return 0 if no device was
    clr r24 ; detected or 1 else
    sbrs r25 ,PA4
    ldi r24 ,0x01
    ret

; ################################################################
;; Προκαλεί καθυστέρηση r25:r24 msec.
wait_msec:
	push r24				; 2 κύκλοι (0.250 μsec)
	push r25				; 2 κύκλοι
	ldi r24,  low(998)		; φόρτωσε τον καταχ. r25:r24 με 998 (1 κύκλος - 0.125 μsec)
	ldi r25,  high(998)		; 1 κύκλος (0.125 μsec)
	rcall wait_usec			; 3 κύκλοι (0.375 μsec), προκαλεί συνολικά καθυστέρηση 998.375 μsec
	pop r25					; 2 κύκλοι (0.250 μsec)
	pop r24					; 2 κύκλοι
	sbiw r24,  1			; 2 κύκλοι
	brne wait_msec			; 1 ή 2 κύκλοι (0.125 ή 0.250 μsec)
	ret						; 4 κύκλοι (0.500 μsec)

;; Προκαλεί καθυστέρηση r25:r24 μsec.
wait_usec:
	sbiw r24, 1				; 2 κύκλοι (0.250 μsec)
	nop						; 1 κύκλος (0.125 μsec)
	nop						; 1 κύκλος (0.125 μsec)
	nop						; 1 κύκλος (0.125 μsec)
	nop						; 1 κύκλος (0.125 μsec)
	brne wait_usec			; 1 ή 2 κύκλοι (0.125 ή 0.250 μsec)
	ret						; 4 κύκλοι (0.500 μsec)

; #################################################################
; screen
; #################################################################
write_2_nibbles:
	push r24				; στέλνει τα 4 MSB
	in r25, PIND			; διαβάζονται τα 4 LSB και τα ξαναστέλνουμε
	andi r25, 0x0f			; για να μην χαλάσουμε την όποια προηγούμενη κατάσταση
	andi r24, 0xf0			; απομονώνονται τα 4 MSB και
	add r24, r25			; συνδυάζονται με τα προϋπάρχοντα 4 LSB
	out PORTD, r24			; και δίνονται στην έξοδο
	sbi PORTD, PD3			; δημιουργείται παλμός Εnable στον ακροδέκτη PD3
	cbi PORTD, PD3			; PD3=1 και μετά PD3=0
	pop r24					; στέλνει τα 4 LSB. Ανακτάται το byte.
	swap r24				; εναλλάσσονται τα 4 MSB με τα 4 LSB
	andi r24, 0xf0			; που με την σειρά τους αποστέλλονται
	add r24, r25
	out PORTD, r24
	sbi PORTD, PD3			; Νέος παλμός Εnable
	cbi PORTD, PD3
	ret

lcd_data:
	sbi PORTD, PD2			; επιλογή του καταχωρήτη δεδομένων (PD2=1)
	rcall write_2_nibbles	; αποστολή του byte
	ldi r24, 43				; αναμονή 43μsec μέχρι να ολοκληρωθεί η λήψη
	ldi r25, 0				; των δεδομένων από τον ελεγκτή της lcd
	rcall wait_usec
	ret

lcd_command:
	cbi PORTD, PD2			; επιλογή του καταχωρητή εντολών (PD2=0)
	rcall write_2_nibbles	; αποστολή της εντολής και αναμονή 39μsec
	ldi r24, 39				; για την ολοκλήρωση της εκτέλεσης της από τον ελεγκτή της lcd.
	ldi r25, 0				; ΣΗΜ.: υπάρχουν δύο εντολές, οι clear display και return home,
	rcall wait_usec			; που απαιτούν σημαντικά μεγαλύτερο χρονικό διάστημα.
	ret

lcd_init:
	ldi r24 ,40 ; Όταν ο ελεγκτής της lcd τροφοδοτείται με
	ldi r25 ,0 ; ρεύμα εκτελεί την δική του αρχικοποίηση.
	rcall wait_msec ; Αναμονή 40 msec μέχρι αυτή να ολοκληρωθεί.
	ldi r24 ,0x30 ; εντολή μετάβασης σε 8 bit mode
	out PORTD ,r24 ; επειδή δεν μπορούμε να είμαστε βέβαιοι
	sbi PORTD ,PD3 ; για τη διαμόρφωση εισόδου του ελεγκτή
	cbi PORTD ,PD3 ; της οθόνης, η εντολή αποστέλλεται δύο φορές
	ldi r24 ,39
	ldi r25 ,0 ; εάν ο ελεγκτής της οθόνης βρίσκεται σε 8-bit mode
	rcall wait_usec ; δεν θα συμβεί τίποτα, αλλά αν ο ελεγκτής έχει διαμόρφωση
	; εισόδου 4 bit θα μεταβεί σε διαμόρφωση 8 bit
	ldi r24 ,0x30
	out PORTD ,r24
	sbi PORTD ,PD3
	cbi PORTD ,PD3
	ldi r24 ,39
	ldi r25 ,0
	rcall wait_usec
	ldi r24 ,0x20 ; αλλαγή σε 4-bit mode
	out PORTD ,r24
	sbi PORTD ,PD3
	cbi PORTD ,PD3
	ldi r24 ,39
	ldi r25 ,0
	rcall wait_usec
	ldi r24 ,0x28 ; επιλογή χαρακτήρων μεγέθους 5x8 κουκίδων
	rcall lcd_command ; και εμφάνιση δύο γραμμών στην οθόνη
	ldi r24 ,0x0c ; ενεργοποίηση της οθόνης, απόκρυψη του κέρσορα
	rcall lcd_command
	ldi r24 ,0x01 ; καθαρισμός της οθόνης
	rcall lcd_command
	ldi r24 ,low(1530)
	ldi r25 ,high(1530)
	rcall wait_usec
	ldi r24 ,0x06 ; ενεργοποίηση αυτόματης αύξησης κατά 1 της διεύθυνσης
	rcall lcd_command ; που είναι αποθηκευμένη στον μετρητή διευθύνσεων και
	; απενεργοποίηση της ολίσθησης ολόκληρης της οθόνης
	ret

; #######################################################
; keyboard
; #######################################################
scan_row:
	ldi r25, 0x08		; αρχικοποίηση με ‘0000 1000’
back_:
	lsl r25				; αριστερή ολίσθηση του ‘1’ τόσες θέσεις
	dec r24				; όσος είναι ο αριθμός της γραμμής
	brne back_
	out PORTC, r25		; η αντίστοιχη γραμμή τίθεται στο λογικό ‘1’
	nop
	nop					; καθυστέρηση για να προλάβει να γίνει η αλλαγή κατάστασης
	in r24, PINC		; επιστρέφουν οι θέσεις (στήλες) των διακοπτών που είναι πιεσμένοι
	andi r24, 0x0f		; απομονώνονται τα 4 LSB όπου τα ‘1’ δείχνουν που είναι πατημένοι
	ret					; οι διακόπτες.


scan_keypad:
	ldi r24, 0x01		; έλεγξε την πρώτη γραμμή του πληκτρολογίου
	rcall scan_row
	swap r24			; αποθήκευσε το αποτέλεσμα
	mov r27, r24		; στα 4 msb του r27
	ldi r24, 0x02		; έλεγξε τη δεύτερη γραμμή του πληκτρολογίου
	rcall scan_row
	add r27, r24		; αποθήκευσε το αποτέλεσμα στα 4 lsb του r27
	ldi r24, 0x03		; έλεγξε την τρίτη γραμμή του πληκτρολογίου
	rcall scan_row
	swap r24			; αποθήκευσε το αποτέλεσμα
	mov r26, r24		; στα 4 msb του r26
	ldi r24, 0x04		; έλεγξε την τέταρτη γραμμή του πληκτρολογίου
	rcall scan_row
	add r26, r24		; αποθήκευσε το αποτέλεσμα στα 4 lsb του r26
	movw r24, r26		; μετέφερε το αποτέλεσμα στους καταχωρητές r25:r24
	ret

scan_keypad_rising_edge:
	mov r22, r24		; αποθήκευσε το χρόνο σπινθηρισμού στον r22
	rcall scan_keypad	; έλεγξε το πληκτρολόγιο για πιεσμένους διακόπτες
	push r24			; και αποθήκευσε το αποτέλεσμα
	push r25
	mov r24, r22		; καθυστέρησε r22 ms (τυπικές τιμές 10-20 msec που καθορίζεται από τον
	ldi r25, 0			; κατασκευαστή του πληκτρολογίου – χρονοδιάρκεια σπινθηρισμών)
	rcall wait_msec
	rcall scan_keypad	; έλεγξε το πληκτρολόγιο ξανά και
	pop r23				; απόρριψε όσα πλήκτρα εμφανίζουν
	pop r22				; σπινθηρισμό
	and r24, r22
	and r25, r23
	ldi r26, low(_tmp_)	; φόρτωσε την κατάσταση των διακοπτών στην
	ldi r27, high(_tmp_) ; προηγούμενη κλήση της ρουτίνας στους r27:r26
	ld r23, X+
	ld r22, X
	st X, r24			; αποθήκευσε στη RAM τη νέα κατάσταση
	st -X, r25			; των διακοπτών
	com r23
	com r22				; βρες τους διακόπτες που έχουν «μόλις» πατηθεί
	and r24, r22
	and r25, r23
	ret

keypad_to_hex: 
movw r26 ,r24
ldi r24 ,'E'
ldi r28, 0xe
sbrc r26 ,0 
ret
ldi r24 ,'0'
ldi r28, 0x0
sbrc r26 ,1 
ret
ldi r24 ,'F'
ldi r28, 0xf
sbrc r26 ,2 
ret
ldi r24 ,'D'
ldi r28, 0xd
sbrc r26 ,3 
ret
ldi r24 ,'7'
ldi r28, 0x7
sbrc r26 ,4 
ret
ldi r24 ,'8'
ldi r28, 0x8
sbrc r26 ,5 
ret
ldi r24 ,'9'
ldi r28, 0x9
sbrc r26 ,6 
ret
ldi r24 ,'C'
ldi r28, 0xc
sbrc r26 ,7 
ret
ldi r24 ,'4'
ldi r28, 0x4 
sbrc r27 ,0 
ret
ldi r24 ,'5'
ldi r28, 0x5 
sbrc r27 ,1 
ret
ldi r24 ,'6'
ldi r28, 0x6 
sbrc r27 ,2 
ret
ldi r24 ,'B'
ldi r28, 0xb 
sbrc r27 ,3 
ret
ldi r24 ,'1'
ldi r28, 0x1 
sbrc r27 ,4 
ret
ldi r24 ,'2'
ldi r28, 0x2 
sbrc r27 ,5 
ret
ldi r24 ,'3'
ldi r28, 0x3 
sbrc r27 ,6 
ret
ldi r24 ,'A'
ldi r28, 0xa 
sbrc r27 ,7 
ret
clr r24 
ret
```


### Κώδικας σε c

Βγάζοντας από τα σχόλια την εντολή #define KEYBOARD παρακάτω μπορούμε να εισάγουμε από το keypad. Οι υπορουτίνες για
διάβασμα από το πληκτρολόγιο και απεικόνισης στην οθόνη είναι από προηγούμενες σειρές. Στην περίπτωση μη ανίχνευσης
συσκευής ή όταν πληκτρολογούμε 0x8000 εμφανίζεται στην οθόνη NO Device. Για αποφυγή εμφάνισης 0 στην οθόνη στην
αρχή του αριθμού (π.χ.+005)κάνουμε κατάλληλο έλεγχο των msdigits.

```c
#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdint.h>
//#define KEYBOARD

#define _NOP() do { __asm__ __volatile__ ("nop"); } while (0)   //orismos kathisterisis

char t[16] = {1,2,3,0xA,4,5,6,0xB,7,8,9,0xC,0xE,0,0xF,0xD};
char to_ascii[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

//
int comp2_to_celcius(uint8_t msbs,uint8_t lsbs)
{
   int16_t celcius = (msbs << 8) + lsbs;
   return celcius / 2;
    
}

int one_wire_reset()
{
	DDRA |= (1 << 4);
	PORTA &= ~(1<<4);
	_delay_us(480);
	DDRA &= ~(1 << 4);
	PORTA &= ~(1<<4);
	_delay_us(100);
	char PA4_digit = PINA & 0x10;  //care only about PA4
	_delay_us(380);
	if(PA4_digit) return 0;
	else return 1;
}

int one_wire_receive_bit()
{
	DDRA |= (1 << 4);
	PORTA &~(1<<4);
	_delay_us(2);
	DDRA &= ~(1 << 4);
	PORTA &= ~(1<<4);
	_delay_us(10);
	char bit = (PINA >> 4) & 1;
 
	_delay_us(49);
	return bit;
 }
 
 int one_wire_receive_byte()
 {
	unsigned int res = 0;
	for(int i = 0;i < 8;i++)
		res = (res >> 1) | (one_wire_receive_bit() << 7);
	return res;
}
 
 void one_wire_transmit_bit(char x)
 {
	DDRA |= (1 << 4);
	PORTA &= ~(1<<4);
	_delay_us(2);
	x = x & 1;
	if(x) PORTA |= (1<<4);
	else PORTA &= ~(1<<4);
	_delay_us(58);
	DDRA &= ~(1 << DDA4);
	PORTA &= ~(1<<4);
	_delay_us(1);
}
 
void one_wire_transmit_byte(char x)
{
	for(int i=0;i<8;i++){
		one_wire_transmit_bit(x & 1);
		x >>= 1;
	}
}



//****************************************************************************

void init_keyboard()
{
    DDRC = 0xf0;
}


int msb(int x)
{
    int res = 0;
    while(x > 1){
        res++;
        x>>=1;
    }
    return res;
}

//only one key pressed at a time
//if nothing pressed: return -1
int scan_keypad()
{
    int key;
    for(int i = 4;i < 8;i++){
        PORTC = 1<<i;
        _NOP();   //kathisterisi gia allagi katastasis
		_NOP();
        key = PINC & 0xf;
        if(key != 0)
            return 4*(i-4)+msb(key);
    }
    return -1;
}
int prev = -1;

int read_spark(){
	int t1,t2;
	do{
		t1 = scan_keypad();
		_delay_ms (16);
		t2 = scan_keypad();
	}while(t1 != t2);
	return t2;
}

int scan_rising_edge()
{
    do{
        int tmp;
        while((tmp = read_spark()) == prev)
            ;
        prev = tmp;
    } while(prev == -1);
	return prev;
}

void pd3_pulse()
{
    char temp = PORTD;
    PORTD = temp | (1<<3);
    PORTD = temp;
}

void write_2_nibbles(char x)
{
    char lsb4 = PORTD & 0x0f;
    PORTD = lsb4 | (x & 0xf0);
    pd3_pulse();
    PORTD = lsb4 | (x << 4);
    pd3_pulse();
}

void lcd_data(char x)
{
    PORTD = PORTD | (1<<2);
    write_2_nibbles(x);
    _delay_us(43);
}

void lcd_command(char x)
{
    PORTD = PORTD & (~(1<<2));
    write_2_nibbles(x);
    _delay_us(39);
}

void lcd_init()
{
    DDRD = 0xff;

    _delay_ms(40);
    PORTD = 0x30;
    pd3_pulse();
    _delay_us(39);
    
    PORTD = 0x30;
    pd3_pulse();
    _delay_us(39);
    
    PORTD = 0x20;
    pd3_pulse();
    _delay_us(39);
    
    lcd_command(0x28);
    lcd_command(0x0c);
    lcd_command(0x01);
    
    _delay_us(1530);
    lcd_command(0x06);
    
}

//****************************************************************
//****************************************************************

int read_temperature()
{
#ifdef KEYBOARD                          //an theloume eisagogi apo keypad 
	int k1 = t[scan_rising_edge()];      //eisagogi twn arithmwn basei tou  char t[16] parapanw 
	int k2 = t[scan_rising_edge()];
	int k3 = t[scan_rising_edge()];
	int k4 = t[scan_rising_edge()];
	int tmp = ((k1 << 12) | (k2 << 8) | (k3 << 4) | k4);     //ftiaxnei ton arithmo
	if(tmp != 0x8000)
		return tmp/2;
	else
		return 0x8000;
	
#else
	int temperature = 0x8000;    //gia mi yparxi syskeuis
	
	
	int detect = one_wire_reset();     //elegxos gia yparxi syskeuis
	if(!detect) return 0x8000;         //an oxi steile 0x8000
	one_wire_transmit_byte(0xCC);        //an yparxi steile tin entoli 0xCC
	one_wire_transmit_byte(0x44);        //meta tin 0x44
	
	while(one_wire_receive_bit());      //perimene mexri na ypologistei h thermokrasia,otan ginei ayto tha stalthei
	//lcd_data('1');                    //apo ti syskeui bit=1 pou to pairnoume me tin one_wire_receive_bit()
	detect = one_wire_reset();          //xana elegxos gia yparxi syskeuis
	if(detect){
		one_wire_transmit_byte(0xCC);      //an yparxei steile tin 0xCC
		one_wire_transmit_byte(0xBE);      //steile tin 0xBE
		uint8_t lsbits = one_wire_receive_byte();  //pairnoume  ta bytes  pou stelnontai
		uint8_t msbits = one_wire_receive_byte();
		temperature = comp2_to_celcius(msbits,lsbits);    //metatropi apo sympliroma tou 2 se bathmous kelsiou
	}
	return temperature;
	
#endif
}

int main()
{
    // 1 is output
	lcd_init();     //arxikopoiisi othonis
#ifdef KEYBOARD
	init_keyboard();      //arxikopoiisi pliktrologiou (4 msb ths C)
#endif

    while(1){
		int temp = read_temperature();
		_delay_ms(100);
		lcd_init();
		
		//an den anixneuthi syskeui steile 0x8000 kai emfanise NO Device
		if(temp == 0x8000){
			lcd_data('N'); lcd_data('O'); lcd_data(' '); lcd_data('D'); lcd_data('e'); lcd_data('v');
			lcd_data('i'); lcd_data('c'); lcd_data('e');
		}
		else{
			if(temp < 0){
				lcd_data('-');
				temp = -temp;
			}else
				lcd_data('+');
			signed char x0 = temp %10;   //lsdigit
			temp /= 10;
			signed char x1 = temp%10;
			temp /= 10;
			signed char x2 = temp;   //msdigit
			
			if(x2 != 0)lcd_data(to_ascii[x2]);              //an to x2 msdigit einai 0 de xreiazetai na emfanistei
			if(x2 != 0 || x1 != 0)lcd_data(to_ascii[x1]);   //stin othoni, omoia gia to x1
			lcd_data(to_ascii[x0]);
			lcd_data(0xB2); // degree 
			lcd_data('C');   //for celcius
		}
	}
}


```