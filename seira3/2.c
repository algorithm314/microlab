#include <avr/io.h>
#define F_CPU 8000000UL
#include <util/delay.h>
#define _NOP() do { __asm__ __volatile__ ("nop"); } while (0)

void init_keyboard()
{
    DDRC = 0xf0;
}

char t[16] = {1,2,3,0xA,4,5,6,0xB,7,8,9,0xC,0xE,0,0xF,0xD};
char to_ascii[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

//x must be != 0
int msb(int x)
{
    int res = 0;
    while(x > 1){
        res++;
        x>>=1;
    }
    return res;
}

//only one key pressed at a time
//if nothing pressed: return -1
int scan_keypad()
{
    int key;
    for(int i = 4;i < 8;i++){
        PORTC = 1<<i;
        _NOP();
		_NOP();
        key = PINC & 0xf;
        if(key != 0)
            return 4*(i-4)+msb(key);
    }
    return -1;
}
int prev = -1;

int read_spark(){
	int t1,t2;
	do{
		t1 = scan_keypad();
		_delay_ms (16);
		t2 = scan_keypad();
	}while(t1 != t2);
	return t2;
}

int scan_rising_edge()
{
    do{
        int tmp;
        while((tmp = read_spark()) == prev)
            ;
        prev = tmp;
    } while(prev == -1);
	return prev;
}

void pd3_pulse()
{
    char temp = PORTD;
    PORTD = temp | (1<<3);
    PORTD = temp;
}

void write_2_nibbles(char x)
{
    char lsb4 = PORTD & 0x0f;
    PORTD = lsb4 | (x & 0xf0);
    pd3_pulse();
    PORTD = lsb4 | (x << 4);
    pd3_pulse();
}

void lcd_data(char x)
{
    PORTD = PORTD | (1<<2);
    write_2_nibbles(x);
    _delay_us(43);
}

void lcd_command(char x)
{
    PORTD = PORTD & (~(1<<2));
    write_2_nibbles(x);
    _delay_us(39);
}

void lcd_init()
{
    DDRD = 0xff;

    _delay_ms(40);
    PORTD = 0x30;
    pd3_pulse();
    _delay_us(39);
    
    PORTD = 0x30;
    pd3_pulse();
    _delay_us(39);
    
    PORTD = 0x20;
    pd3_pulse();
    _delay_us(39);
    
    lcd_command(0x28);
    lcd_command(0x0c);
    lcd_command(0x01);
    
    _delay_us(1530);
    lcd_command(0x06);
    
}

int main()
{
    DDRB = 0xff;
    init_keyboard();
	lcd_init();
    while(1){

    	int k1 = t[scan_rising_edge()];
        int k2 = t[scan_rising_edge()];
		lcd_init();
		// print hex
		lcd_data(to_ascii[k1]);
		lcd_data(to_ascii[k2]);
		lcd_data('=');
		signed char num = (k1 << 4) | k2;
		if(num < 0){
			lcd_data('-');
			if(num == -128){
				lcd_data('1');
				lcd_data('2');
				lcd_data('8');
				continue;
			}
			num *= -1;
		}else{
			lcd_data('+');
		}
		signed char x0 = num %10;
		num /= 10;
		signed char x1 = num%10;
		num /= 10;
		signed char x2 = num;

		lcd_data(to_ascii[x2]);
		lcd_data(to_ascii[x1]);
		lcd_data(to_ascii[x0]);
	}
}
