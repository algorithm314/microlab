//3.c

#include <avr/io.h>
#define F_CPU 8000000UL
#include <util/delay.h>
#define _NOP() do { __asm__ __volatile__ ("nop"); } while (0)



void pd3_pulse()
{
	char temp = PORTD;
	PORTD = temp | (1<<3);
	PORTD = temp;
}

void write_2_nibbles(char x)
{
	char lsb4 = PORTD & 0x0f;
	PORTD = lsb4 | (x & 0xf0);
	pd3_pulse();
	PORTD = lsb4 | (x << 4);
	pd3_pulse();
}

void lcd_data(char x)
{
	PORTD = PORTD | (1<<2);
	write_2_nibbles(x);
	_delay_us(43);
}

void lcd_command(char x)
{
	PORTD = PORTD & (~(1<<2));
	write_2_nibbles(x);
	_delay_us(39);
}

void lcd_init()
{
	DDRD = 0xff;

	_delay_ms(40);
	PORTD = 0x30;
	pd3_pulse();
	_delay_us(39);

	PORTD = 0x30;
	pd3_pulse();
	_delay_us(39);

	PORTD = 0x20;
	pd3_pulse();
	_delay_us(39);

	lcd_command(0x28);
	lcd_command(0x0c);
	lcd_command(0x01);

	_delay_us(1530);
	lcd_command(0x06);

}


//***********************************



int monades_second=0;
int dekades_second=0;
int monades_minute=0;
int dekades_minute=0;



char message[7]={0x3030,0x4d20,0x4e49,0x303a,0x2030,0x4553,0x0143};


//initialize_message
int counter=13;
lcd_command(0x80);


void myloop(){


}



void counter_increment(){
	monades_second++;
	if(monades_second==10){
		monades_second=0;
		dekades_second++;
		if(dekades_second==6){
			dekades_second=0;
			monades_minute++;
			if(monades_minute==10){
				monades_minute=0;
				dekades_minute++;
				if(dekades_minute==6){
					dekades_minute=0;
				}

			}

		}


	}


}


void counter_to_lcd(){
	lcd_command(0x80);
	lcd_data(dekades_minute);
	lcd_data(monades_minute);

	lcd_command(0x87);
	lcd_data(dekades_second);
	lcd_data(monades_second);

}



int main(){
	DDRD=0xff;

	DDRB=0x00;



	lcd_init();

	//initialize_message


	while(1){
		int temp;
		temp= PINB&0x07;
		if(temp) initialize_message();

		temp= PINB&0x00;
		if(!temp) continue;

		counter_increment();
		counter_to_lcd();

		_delay_ms(1000);



	}
}
