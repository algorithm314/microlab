---
mainfont: Open Sans
documentclass: extarticle
fontsize: 12pt
geometry: margin=1in
papersize: a4
title: "3η Σειρά ασκήσεων, Εργαστήριο Μικροϋπολογιστών"
author:
- "Αγιάννης Κωνσταντίνος 03116352"
- "Τσιαχρήστος Δημήτριος 03116048"
---

# Ζήτημα 3.1 

### Κώδικας σε asm

Στο correct με τα 07 ψηφία αναβουν ολα τα Β ενω με άλλα αναβοσβήνουν οπως φαινεται στο times8.
Διαβάζουμε με τη scan_key που εχει σπινθηρισμο. Το 0 ειναι στο first_key και το 7 στο second_key.


```asm
.include "m16def.inc"
; omada 07

#define first_key (1 << 1)    ;orismos 1ou psifiou
#define second_key (1 << 4)   ;2ou psifiou
#define one r3
#define zero r2
#define tmp r30

ldi r24, low(RAMEND)
out SPL, r24
ldi r24, high(RAMEND)
out SPH, r24 ; αρχικοποίηση stack pointer

ser r24
out DDRD,r24 ; αρχικοποίηση PORTD που συνδέεται η οθόνη, ως έξοδος
out DDRB,r24 ; r24 έξοδος

clr r2; 0
clr r3
dec r3; 0xff

ldi r24 ,(1 << PC7) | (1 << PC6) | (1 << PC5) | (1 << PC4) ; θέτει ως εξόδους τα 4 MSB
out DDRC ,r24 ; της θύρας PORTC


loop:
	rcall scan_key 
	cpi r24,first_key       ;elegxos an patithike to 0
	brne one_remainning
	rcall scan_key
	cpi r24,second_key      ;elegxos an patithike to 7
	brne wrong 
correct:
	ldi r24,LOW(4000)     ;gia 4 sec
	ldi r25,HIGH(4000)
	out PORTB,one         ;anapse ola ta B
	rcall wait_msec
	out PORTB,zero
	jmp loop               ;synexous leitourgias
one_remainning:
	rcall scan_key
wrong:
	

	ldi tmp,8
times8:                  ;lathos psifia opote tha anabosbinoun

	out PORTB,one          ;anaboun ta B gia
	ldi r24,LOW(250)      ;0.25sec
	ldi r25,HIGH(250)
	rcall wait_msec
	out PORTB,zero         ;sbinoun ta B gia
	ldi r24,LOW(250)        ;0.25sec
	ldi r25,HIGH(250)
	rcall wait_msec
	dec tmp
	brne times8             ;gia na anabosbinoun 8 fores
	jmp loop                ;otan teleiosei gia synexomeni leitourgia


scan_key:        ;gia na diavazei apo pliktrologio
	ldi r24,20; ms για σπινθιρισμό
	rcall scan_keypad_rising_edge
	clr tmp
	or tmp,r24
	or tmp,r25
	cpi tmp,0
	breq scan_key
	ret	


scan_row:
	ldi r25 , 0x08 ; αρχικοποίηση με ‘0000 1000’	
back_: 
	lsl r25 ; αριστερή ολίσθηση του ‘1’ τόσες θέσεις
	dec r24 ; όσος είναι ο αριθμός της γραμμής
	brne back_
	out PORTC , r25 ; η αντίστοιχη γραμμή τίθεται στο λογικό ‘1’
	nop
	nop ; καθυστέρηση για να προλάβει να γίνει η αλλαγή κατάστασης
	in r24 , PINC ; επιστρέφουν οι θέσεις (στήλες) των διακοπτών που είναι πιεσμένοι
	andi r24 ,0x0f ; απομονώνονται τα 4 LSB όπου τα ‘1’ δείχνουν που είναι πατημένοι
	ret ; οι διακόπτες.

scan_keypad:
	ldi r24 , 0x01 ; έλεγξε την πρώτη γραμμή του πληκτρολογίου
	rcall scan_row
	swap r24 ; αποθήκευσε το αποτέλεσμα
	mov r27 , r24 ; στα 4 msb του r27
	ldi r24 ,0x02 ; έλεγξε τη δεύτερη γραμμή του πληκτρολογίου
	rcall scan_row
	add r27 , r24 ; αποθήκευσε το αποτέλεσμα στα 4 lsb του r27
	ldi r24 , 0x03 ; έλεγξε την τρίτη γραμμή του πληκτρολογίου
	rcall scan_row
	swap r24 ; αποθήκευσε το αποτέλεσμα
	mov r26 , r24 ; στα 4 msb του r26
	ldi r24 ,0x04 ; έλεγξε την τέταρτη γραμμή του πληκτρολογίου
	rcall scan_row
	add r26 , r24 ; αποθήκευσε το αποτέλεσμα στα 4 lsb του r26
	movw r24 , r26 ; μετέφερε το αποτέλεσμα στους καταχωρητές r25:r24
	ret

.DSEG
_tmp_: .byte 2
; ---- Τέλος τμήματος δεδομένων
.CSEG
scan_keypad_rising_edge:
	mov r22 ,r24 ; αποθήκευσε το χρόνο σπινθηρισμού στον r22
	rcall scan_keypad ; έλεγξε το πληκτρολόγιο για πιεσμένους διακόπτες
	push r24 ; και αποθήκευσε το αποτέλεσμα
	push r25
	mov r24 ,r22 ; καθυστέρησε r22 ms (τυπικές τιμές 10-20 msec που καθορίζεται από τον
	ldi r25 ,0 ; κατασκευαστή του πληκτρολογίου – χρονοδιάρκεια σπινθηρισμών)
	rcall wait_msec
	rcall scan_keypad ; έλεγξε το πληκτρολόγιο ξανά και απόρριψε
	pop r23 ; όσα πλήκτρα εμφανίζουν σπινθηρισμό
	pop r22
	and r24 ,r22
	and r25 ,r23
	ldi r26 ,low(_tmp_) ; φόρτωσε την κατάσταση των διακοπτών στην
	ldi r27 ,high(_tmp_) ; προηγούμενη κλήση της ρουτίνας στους r27:r26
	ld r23 ,X+
	ld r22 ,X
	st X ,r24 ; αποθήκευσε στη RAM τη νέα κατάσταση
	st -X ,r25 ; των διακοπτών
	com r23
	com r22 ; βρες τους διακόπτες που έχουν «μόλις» πατηθεί
	and r24 ,r22
	and r25,r23
	ret


; ------------------------------------------
; LCD functions
; ------------------------------------------

write_2_nibbles:
	push r24 ; στέλνει τα 4 MSB
	in r25 ,PIND ; διαβάζονται τα 4 LSB και τα ξαναστέλνουμε
	andi r25 ,0x0f ; για να μην χαλάσουμε την όποια προηγούμενη κατάσταση
	andi r24 ,0xf0 ; απομονώνονται τα 4 MSB και
	add r24 ,r25 ; συνδυάζονται με τα προϋπάρχοντα 4 LSB
	out PORTD ,r24 ; και δίνονται στην έξοδο
	sbi PORTD ,PD3 ; δημιουργείται παλμός Enable στον ακροδέκτη PD3
	cbi PORTD ,PD3 ; PD3=1 και μετά PD3=0
	pop r24 ; στέλνει τα 4 LSB. Ανακτάται το byte.
	swap r24 ; εναλλάσσονται τα 4 MSB με τα 4 LSB
	andi r24 ,0xf0 ; που με την σειρά τους αποστέλλονται
	add r24 ,r25
	out PORTD ,r24
	sbi PORTD ,PD3 ; Νέος παλμός Enable
	cbi PORTD ,PD3
	ret


lcd_data:
	sbi PORTD ,PD2 ; επιλογή του καταχωρητή δεδομένων (PD2=1)
	rcall write_2_nibbles ; αποστολή του byte
	ldi r24 ,43 ; αναμονή 43μsec μέχρι να ολοκληρωθεί η λήψη
	ldi r25 ,0 ; των δεδομένων από τον ελεγκτή της lcd
	rcall wait_usec
	ret

lcd_command:
	cbi PORTD ,PD2 ; επιλογή του καταχωρητή εντολών (PD2=1)
	rcall write_2_nibbles ; αποστολή της εντολής και αναμονή 39μsec
	ldi r24 ,39 ; για την ολοκλήρωση της εκτέλεσης της από τον ελεγκτή της lcd.
	ldi r25 ,0 ; ΣΗΜ.: υπάρχουν δύο εντολές, οι clear display και return home,
	rcall wait_usec ; που απαιτούν σημαντικά μεγαλύτερο χρονικό διάστημα.
	ret

lcd_init:
	ldi r24 ,40 ; Όταν ο ελεγκτής της lcd τροφοδοτείται με
	ldi r25 ,0 ; ρεύμα εκτελεί την δική του αρχικοποίηση.
	rcall wait_msec ; Αναμονή 40 msec μέχρι αυτή να ολοκληρωθεί.
	ldi r24 ,0x30 ; εντολή μετάβασης σε 8 bit mode
	out PORTD ,r24 ; επειδή δεν μπορούμε να είμαστε βέβαιοι
	sbi PORTD ,PD3 ; για τη διαμόρφωση εισόδου του ελεγκτή
	cbi PORTD ,PD3 ; της οθόνης, η εντολή αποστέλλεται δύο φορές
	ldi r24 ,39
	ldi r25 ,0 ; εάν ο ελεγκτής της οθόνης βρίσκεται σε 8-bit mode
	rcall wait_usec ; δεν θα συμβεί τίποτα, αλλά αν ο ελεγκτής έχει διαμόρφωση
	; εισόδου 4 bit θα μεταβεί σε διαμόρφωση 8 bit
	ldi r24 ,0x30
	out PORTD ,r24
	sbi PORTD ,PD3
	cbi PORTD ,PD3
	ldi r24 ,39
	ldi r25 ,0
	rcall wait_usec
	ldi r24 ,0x20 ; αλλαγή σε 4-bit mode
	out PORTD ,r24
	sbi PORTD ,PD3
	cbi PORTD ,PD3
	ldi r24 ,39
	ldi r25 ,0
	rcall wait_usec
	ldi r24 ,0x28 ; επιλογή χαρακτήρων μεγέθους 5x8 κουκίδων
	rcall lcd_command ; και εμφάνιση δύο γραμμών στην οθόνη
	ldi r24 ,0x0c ; ενεργοποίηση της οθόνης, απόκρυψη του κέρσορα
	rcall lcd_command
	ldi r24 ,0x01 ; καθαρισμός της οθόνης
	rcall lcd_command
	ldi r24 ,low(1530)
	ldi r25 ,high(1530)
	rcall wait_usec
	ldi r24 ,0x06 ; ενεργοποίηση αυτόματης αύξησης κατά 1 της διεύθυνσης
	rcall lcd_command ; που είναι αποθηκευμένη στον μετρητή διευθύνσεων και
	; απενεργοποίηση της ολίσθησης ολόκληρης της οθόνης
	ret


wait_msec:
	;ret ;DEGUG
	push r24
	push r25
	ldi r24 , low(998) 
	ldi r25 , high(998) 
	rcall wait_usec 
	pop r25 
	pop r24 
	sbiw r24 , 1 
	brne wait_msec 
	ret 
wait_usec:
	sbiw r24 ,1 
	nop 
	nop 
	nop 
	nop 
	brne wait_usec 
	ret
```

### Κώδικας σε C

Με τη read_spark ελεγχουμε για σπινθηριμο.  Με τη scan_keypad φτιαχνουμε τον αριθμο βασει του ψηφιου που 
πατηθηκε καθε φορα. Διαβαζουμε με τη scan_rising_edge που χρησιμοποιει τις προηγουμενες 2.

```c
#include <avr/io.h>
#define F_CPU 8000000UL
#include <util/delay.h>
#define _NOP() do { __asm__ __volatile__ ("nop"); } while (0)

void init_keyboard()
{
    DDRC = 0xf0;
}

//x must be != 0
int msb(int x)
{
    int res = 0;
    while(x > 1){
        res++;
        x>>=1;
    }
    return res;
}

//only one key pressed at a time
//if nothing pressed: return -1
int scan_keypad()
{
    int key;
    for(int i = 4;i < 8;i++){
        PORTC = 1<<i;
        _NOP();
		_NOP();
        key = PINC & 0xf;
        if(key != 0)
            return 4*(i-4)+msb(key);
    }
    return -1;
}
int prev = -1;

int read_spark(){
	int t1,t2;
	do{
		t1 = scan_keypad();
		_delay_ms (16);
		t2 = scan_keypad();
	}while(t1 != t2);
	return t2;
}
int scan_rising_edge()
{
    do{
        int tmp;
        while((tmp = read_spark()) == prev)
            ;
        prev = tmp;
    } while(prev == -1);
	return prev;
}

int main()
{
    DDRB = 0xff;
    init_keyboard();
    while(1){
        int k1 = scan_rising_edge();      ;diavasma 1ou psifiou 
        int k2 = scan_rising_edge();      ;diavasma 2ou psifiou 
        if(k1 == 13 && k2 == 8){
            PORTB = 0xff;                   ;anaboun ola gia 4sec an 07
            _delay_ms (4000);
			PORTB = 0;
        }else{
            for(int i = 0;i < 8;i++){        ;anabosvinei 8 fores
                PORTB = 0xff;                 ;ola tis B
                _delay_ms (250);             ;gia 0.25sec on
                PORTB = 0;
                 _delay_ms (250);            ;gia 0.25sec off
            }
                
        }
        
    }
}

```

# Ζήτημα 3.2

### Κώδικας σε ASM

Στο τέλος χρησιμοποιούνται 3 πίνακες για τα ψηφία που προέέέκυψαν από το terminal με for(i<129.. )
prin(i mod 10),  print((i//10) mod 10), print((i//100)mod 10 )  αντίστοιχα για τη μετατροπή από 16δικο σε 10δικο.
Στην keypad_to_hex υπάρχει επιπλέον ο καταχωρητής r28 για να κρατάει τη 16δική μορφή του ψηφιου που πατηθηκε.


```asm
.include "m16def.inc"

#define first_key (1 << 1)
#define second_key (1 << 4)
#define one r3
#define zero r2
#define tmp r19


ldi r24, low(RAMEND) 
out SPL, r24
ldi r24, high(RAMEND)
out SPH, r24 
ser r24
out DDRD, r24 
clr r24

ser r24
out DDRD,r24 ; αρχικοποίηση PORTD που συνδέεται η οθόνη, ως έξοδος
out DDRB,r24 ; r24 έξοδος

clr r2; 0
clr r3
dec r3; 0xff

ldi r24 ,(1 << PC7) | (1 << PC6) | (1 << PC5) | (1 << PC4) ; θέτει ως εξόδους τα 4 MSB
out DDRC ,r24 ; της θύρας PORTC


push r24
push r25
rcall lcd_init
pop r25
pop r24

main:


rcall  scan_key
;movw r29,r24
rcall keypad_to_hex
mov r21,r24 //r21 first ascii
mov r18,r28 //r18 first num


rcall  scan_key

; clear screen
push r24    ;push kai pop epeidi xrisimopoiountai kai mesa stin lcd_init
push r25
rcall lcd_init
pop r25
pop r24

;movw r27,r24
rcall  keypad_to_hex

mov r29,r24 // r29
mov r24,r21
rcall lcd_data
mov r24,r29
rcall  lcd_data

ldi r24,'='
rcall lcd_data

// create the number
lsl r18
lsl r18
lsl r18
lsl r18
or r18,r28 // now r18 has the number

cpi r18,0     ;elegxos gia arnitika
brmi minus
ldi r24,'+'
rcall lcd_data

positive:
	ldi zl,LOW(ekatontades*2)     ;ekatontades,monades,dekades stous pinakes parakato
	ldi zh,HIGH(ekatontades*2)
	add zl,r18
	adc zh,zero
	lpm r24,Z
	rcall lcd_data

	ldi zl,LOW(dekades*2)
	ldi zh,HIGH(dekades*2)
	add zl,r18
	adc zh,zero
	lpm r24,Z
	rcall lcd_data

	ldi zl,LOW(monades*2)
	ldi zh,HIGH(monades*2)
	add zl,r18
	adc zh,zero
	lpm r24,Z
	rcall lcd_data
jmp main

minus:
ldi r24,'-'
rcall lcd_data
cpi r18,-128
brne not_128

ldi r24,'1'       ;ksexoristi periptosi gia to -128 
rcall lcd_data
ldi r24,'2'
rcall lcd_data
ldi r24,'8'
rcall lcd_data
jmp main

not_128:
neg r18

jmp positive
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

keypad_to_hex: 
movw r26 ,r24
ldi r24 ,'E'
ldi r28, 0xe    ;gia ti 16diki morfi tou psifiou
sbrc r26 ,0 
ret
ldi r24 ,'0'
ldi r28, 0x0
sbrc r26 ,1 
ret
ldi r24 ,'F'
ldi r28, 0xf
sbrc r26 ,2 
ret
ldi r24 ,'D'
ldi r28, 0xd
sbrc r26 ,3 
ret
ldi r24 ,'7'
ldi r28, 0x7
sbrc r26 ,4 
ret
ldi r24 ,'8'
ldi r28, 0x8
sbrc r26 ,5 
ret
ldi r24 ,'9'
ldi r28, 0x9
sbrc r26 ,6 
ret
ldi r24 ,'C'
ldi r28, 0xc
sbrc r26 ,7 
ret
ldi r24 ,'4'
ldi r28, 0x4 
sbrc r27 ,0 
ret
ldi r24 ,'5'
ldi r28, 0x5 
sbrc r27 ,1 
ret
ldi r24 ,'6'
ldi r28, 0x6 
sbrc r27 ,2 
ret
ldi r24 ,'B'
ldi r28, 0xb 
sbrc r27 ,3 
ret
ldi r24 ,'1'
ldi r28, 0x1 
sbrc r27 ,4 
ret
ldi r24 ,'2'
ldi r28, 0x2 
sbrc r27 ,5 
ret
ldi r24 ,'3'
ldi r28, 0x3 
sbrc r27 ,6 
ret
ldi r24 ,'A'
ldi r28, 0xa 
sbrc r27 ,7 
ret
clr r24 
ret


scan_key:
	ldi r24,20; ms για σπινθιρισμό
	rcall scan_keypad_rising_edge
	clr tmp
	or tmp,r24
	or tmp,r25
	cpi tmp,0
	breq scan_key
	ret	


scan_row:
	ldi r25 , 0x08 ; αρχικοποίηση με ‘0000 1000’	
back_: 
	lsl r25 ; αριστερή ολίσθηση του ‘1’ τόσες θέσεις
	dec r24 ; όσος είναι ο αριθμός της γραμμής
	brne back_
	out PORTC , r25 ; η αντίστοιχη γραμμή τίθεται στο λογικό ‘1’
	nop
	nop ; καθυστέρηση για να προλάβει να γίνει η αλλαγή κατάστασης
	in r24 , PINC ; επιστρέφουν οι θέσεις (στήλες) των διακοπτών που είναι πιεσμένοι
	andi r24 ,0x0f ; απομονώνονται τα 4 LSB όπου τα ‘1’ δείχνουν που είναι πατημένοι
	ret ; οι διακόπτες.

scan_keypad:
	ldi r24 , 0x01 ; έλεγξε την πρώτη γραμμή του πληκτρολογίου
	rcall scan_row
	swap r24 ; αποθήκευσε το αποτέλεσμα
	mov r27 , r24 ; στα 4 msb του r27
	ldi r24 ,0x02 ; έλεγξε τη δεύτερη γραμμή του πληκτρολογίου
	rcall scan_row
	add r27 , r24 ; αποθήκευσε το αποτέλεσμα στα 4 lsb του r27
	ldi r24 , 0x03 ; έλεγξε την τρίτη γραμμή του πληκτρολογίου
	rcall scan_row
	swap r24 ; αποθήκευσε το αποτέλεσμα
	mov r26 , r24 ; στα 4 msb του r26
	ldi r24 ,0x04 ; έλεγξε την τέταρτη γραμμή του πληκτρολογίου
	rcall scan_row
	add r26 , r24 ; αποθήκευσε το αποτέλεσμα στα 4 lsb του r26
	movw r24 , r26 ; μετέφερε το αποτέλεσμα στους καταχωρητές r25:r24
	ret

.DSEG
_tmp_: .byte 2
; ---- Τέλος τμήματος δεδομένων
.CSEG
scan_keypad_rising_edge:
	mov r22 ,r24 ; αποθήκευσε το χρόνο σπινθηρισμού στον r22
	rcall scan_keypad ; έλεγξε το πληκτρολόγιο για πιεσμένους διακόπτες
	push r24 ; και αποθήκευσε το αποτέλεσμα
	push r25
	mov r24 ,r22 ; καθυστέρησε r22 ms (τυπικές τιμές 10-20 msec που καθορίζεται από τον
	ldi r25 ,0 ; κατασκευαστή του πληκτρολογίου – χρονοδιάρκεια σπινθηρισμών)
	rcall wait_msec
	rcall scan_keypad ; έλεγξε το πληκτρολόγιο ξανά και απόρριψε
	pop r23 ; όσα πλήκτρα εμφανίζουν σπινθηρισμό
	pop r22
	and r24 ,r22
	and r25 ,r23
	ldi r26 ,low(_tmp_) ; φόρτωσε την κατάσταση των διακοπτών στην
	ldi r27 ,high(_tmp_) ; προηγούμενη κλήση της ρουτίνας στους r27:r26
	ld r23 ,X+
	ld r22 ,X
	st X ,r24 ; αποθήκευσε στη RAM τη νέα κατάσταση
	st -X ,r25 ; των διακοπτών
	com r23
	com r22 ; βρες τους διακόπτες που έχουν «μόλις» πατηθεί
	and r24 ,r22
	and r25,r23
	ret


; ------------------------------------------
; LCD functions
; ------------------------------------------

write_2_nibbles:
	push r24 ; στέλνει τα 4 MSB
	in r25 ,PIND ; διαβάζονται τα 4 LSB και τα ξαναστέλνουμε
	andi r25 ,0x0f ; για να μην χαλάσουμε την όποια προηγούμενη κατάσταση
	andi r24 ,0xf0 ; απομονώνονται τα 4 MSB και
	add r24 ,r25 ; συνδυάζονται με τα προϋπάρχοντα 4 LSB
	out PORTD ,r24 ; και δίνονται στην έξοδο
	sbi PORTD ,PD3 ; δημιουργείται παλμός Enable στον ακροδέκτη PD3
	cbi PORTD ,PD3 ; PD3=1 και μετά PD3=0
	pop r24 ; στέλνει τα 4 LSB. Ανακτάται το byte.
	swap r24 ; εναλλάσσονται τα 4 MSB με τα 4 LSB
	andi r24 ,0xf0 ; που με την σειρά τους αποστέλλονται
	add r24 ,r25
	out PORTD ,r24
	sbi PORTD ,PD3 ; Νέος παλμός Enable
	cbi PORTD ,PD3
	ret


lcd_data:
	sbi PORTD ,PD2 ; επιλογή του καταχωρητή δεδομένων (PD2=1)
	rcall write_2_nibbles ; αποστολή του byte
	ldi r24 ,43 ; αναμονή 43μsec μέχρι να ολοκληρωθεί η λήψη
	ldi r25 ,0 ; των δεδομένων από τον ελεγκτή της lcd
	rcall wait_usec
	ret

lcd_command:
	cbi PORTD ,PD2 ; επιλογή του καταχωρητή εντολών (PD2=1)
	rcall write_2_nibbles ; αποστολή της εντολής και αναμονή 39μsec
	ldi r24 ,39 ; για την ολοκλήρωση της εκτέλεσης της από τον ελεγκτή της lcd.
	ldi r25 ,0 ; ΣΗΜ.: υπάρχουν δύο εντολές, οι clear display και return home,
	rcall wait_usec ; που απαιτούν σημαντικά μεγαλύτερο χρονικό διάστημα.
	ret

lcd_init:
	ldi r24 ,40 ; Όταν ο ελεγκτής της lcd τροφοδοτείται με
	ldi r25 ,0 ; ρεύμα εκτελεί την δική του αρχικοποίηση.
	rcall wait_msec ; Αναμονή 40 msec μέχρι αυτή να ολοκληρωθεί.
	ldi r24 ,0x30 ; εντολή μετάβασης σε 8 bit mode
	out PORTD ,r24 ; επειδή δεν μπορούμε να είμαστε βέβαιοι
	sbi PORTD ,PD3 ; για τη διαμόρφωση εισόδου του ελεγκτή
	cbi PORTD ,PD3 ; της οθόνης, η εντολή αποστέλλεται δύο φορές
	ldi r24 ,39
	ldi r25 ,0 ; εάν ο ελεγκτής της οθόνης βρίσκεται σε 8-bit mode
	rcall wait_usec ; δεν θα συμβεί τίποτα, αλλά αν ο ελεγκτής έχει διαμόρφωση
	; εισόδου 4 bit θα μεταβεί σε διαμόρφωση 8 bit
	ldi r24 ,0x30
	out PORTD ,r24
	sbi PORTD ,PD3
	cbi PORTD ,PD3
	ldi r24 ,39
	ldi r25 ,0
	rcall wait_usec
	ldi r24 ,0x20 ; αλλαγή σε 4-bit mode
	out PORTD ,r24
	sbi PORTD ,PD3
	cbi PORTD ,PD3
	ldi r24 ,39
	ldi r25 ,0
	rcall wait_usec
	ldi r24 ,0x28 ; επιλογή χαρακτήρων μεγέθους 5x8 κουκίδων
	rcall lcd_command ; και εμφάνιση δύο γραμμών στην οθόνη
	ldi r24 ,0x0c ; ενεργοποίηση της οθόνης, απόκρυψη του κέρσορα
	rcall lcd_command
	ldi r24 ,0x01 ; καθαρισμός της οθόνης
	rcall lcd_command
	ldi r24 ,low(1530)
	ldi r25 ,high(1530)
	rcall wait_usec
	ldi r24 ,0x06 ; ενεργοποίηση αυτόματης αύξησης κατά 1 της διεύθυνσης
	rcall lcd_command ; που είναι αποθηκευμένη στον μετρητή διευθύνσεων και
	; απενεργοποίηση της ολίσθησης ολόκληρης της οθόνης
	ret


wait_msec:
	;ret ;DEGUG
	push r24
	push r25
	ldi r24 , low(998) 
	ldi r25 , high(998) 
	rcall wait_usec 
	pop r25 
	pop r24 
	sbiw r24 , 1 
	brne wait_msec 
	ret 
wait_usec:
	sbiw r24 ,1 
	nop 
	nop 
	nop 
	nop 
	brne wait_usec 
	ret
monades: 
.db '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8'
dekades:
.db '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '3', '3', '3', '3', '3', '3', '3', '3', '3', '3', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '6', '6', '6', '6', '6', '6', '6', '6', '6', '6', '7', '7', '7', '7', '7', '7', '7', '7', '7', '7', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '2', '2', '2', '2', '2', '2', '2', '2', '2'
ekatontades:
.db '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'

```

### Κώδικας σε C

Ξεχωριστη περιπτωση για το -128. Στον to_ascii ειναι το αντιστοιχα 16δικα ψηφια του t του keypad που θα εμφανιστουν 
στην οθονη.

```c


#include <avr/io.h>
#define F_CPU 8000000UL
#include <util/delay.h>
#define _NOP() do { __asm__ __volatile__ ("nop"); } while (0)

void init_keyboard()
{
    DDRC = 0xf0;
}

char t[16] = {1,2,3,0xA,4,5,6,0xB,7,8,9,0xC,0xE,0,0xF,0xD};
char to_ascii[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

//x must be != 0
int msb(int x)
{
    int res = 0;
    while(x > 1){
        res++;
        x>>=1;
    }
    return res;
}

//only one key pressed at a time
//if nothing pressed: return -1
int scan_keypad()
{
    int key;
    for(int i = 4;i < 8;i++){
        PORTC = 1<<i;
        _NOP();
		_NOP();
        key = PINC & 0xf;
        if(key != 0)
            return 4*(i-4)+msb(key);
    }
    return -1;
}
int prev = -1;

int read_spark(){
	int t1,t2;
	do{
		t1 = scan_keypad();
		_delay_ms (16);
		t2 = scan_keypad();
	}while(t1 != t2);
	return t2;
}

int scan_rising_edge()
{
    do{
        int tmp;
        while((tmp = read_spark()) == prev)
            ;
        prev = tmp;
    } while(prev == -1);
	return prev;
}

void pd3_pulse()
{
    char temp = PORTD;
    PORTD = temp | (1<<3);
    PORTD = temp;
}

void write_2_nibbles(char x)
{
    char lsb4 = PORTD & 0x0f;
    PORTD = lsb4 | (x & 0xf0);
    pd3_pulse();
    PORTD = lsb4 | (x << 4);
    pd3_pulse();
}

void lcd_data(char x)
{
    PORTD = PORTD | (1<<2);
    write_2_nibbles(x);
    _delay_us(43);
}

void lcd_command(char x)
{
    PORTD = PORTD & (~(1<<2));
    write_2_nibbles(x);
    _delay_us(39);
}

void lcd_init()
{
    DDRD = 0xff;

    _delay_ms(40);
    PORTD = 0x30;
    pd3_pulse();
    _delay_us(39);
    
    PORTD = 0x30;
    pd3_pulse();
    _delay_us(39);
    
    PORTD = 0x20;
    pd3_pulse();
    _delay_us(39);
    
    lcd_command(0x28);
    lcd_command(0x0c);
    lcd_command(0x01);
    
    _delay_us(1530);
    lcd_command(0x06);
    
}

int main()
{
    DDRB = 0xff;
    init_keyboard();
	lcd_init();
    while(1){

    	int k1 = t[scan_rising_edge()];           ;diavasma 1ou psifiou 
        int k2 = t[scan_rising_edge()];            ;2ou
		lcd_init();
		// print hex
		lcd_data(to_ascii[k1]);                      ;emfanisi stin otoni tou antistoixou 16dikou
		lcd_data(to_ascii[k2]);
		lcd_data('=');
		signed char num = (k1 << 4) | k2;     ;olisthisi tou 1ou psifiou 4 fores gia ftiaksimo telikou arithmou
		if(num < 0){
			lcd_data('-');
			if(num == -128){
				lcd_data('1');
				lcd_data('2');
				lcd_data('8');
				continue;
			}
			num *= -1;
		}else{
			lcd_data('+');
		}
		signed char x0 = num %10;
		num /= 10;
		signed char x1 = num%10;
		num /= 10;
		signed char x2 = num;
                                      ;stin othoni
		lcd_data(to_ascii[x2]);      ;ekatontades
		lcd_data(to_ascii[x1]);      ;dekades
		lcd_data(to_ascii[x0]);      ;monades
	}
}
```

# Ζήτημα 3.3

### Κώδικας σε ASM

 Στο τέλος στον πίνακα message αρχικοποιηση του αρχικού μηνύματος. To PB7 εχει μεγαλυτερη προτεραιοτητα 
 απο το PB0 γι αυτο ελεγχεται πριν απο το PB0 στο συνεχομενο loop (eternal_loop). Στο counter_increment:
 η διαδικασια του χρονομετρου.
 

```asm
.include "m16def.inc"
.def monades_minute = r19
.def dekades_minute = r20
.def monades_second = r21
.def dekades_second = r22
.def temp = r16
.def counter = r17
.def input = r18

jmp main

;***********************************************************************************************

initialize_message:
	ldi zh, high(Message*2)
	ldi zl, low(Message*2)
	ldi counter, 13
	
	ldi r24, 0x80
	rcall lcd_command
myloop:
	lpm r24, Z
	rcall lcd_data
	adiw zl, 1
	dec counter
	brne myloop
	
	clr monades_second
	clr dekades_second
	clr monades_minute
	clr dekades_minute
	ret

counter_increment:
	inc monades_second					; genikh morfh:  "dekades_minute monades_minute MIN: dekades_second monades_second SEC"
	cpi monades_second, 0x0a           ;otan ftasei to 10 midenise monades_sec kai inc dekades_sec 
	brne end_incr                       ;allios teleiose i metrisi

	clr monades_second
	inc dekades_second
	cpi dekades_second, 6            ;otan ftasei to 6 midenise dekades_sec kai inc monades_min
	brne end_incr                    ;allios teleiose i metrisi

	clr dekades_second
	inc monades_minute
	cpi monades_minute, 0x0a        ;omoia me prin
	brne end_incr
	
	clr monades_minute
	inc dekades_minute
	cpi dekades_minute, 6
	brne end_incr

	clr dekades_minute

end_incr:
	ret      ;epistofi se kyrio programma

counter_to_lcd:
	ori monades_second, 0x30			;oloi oi arithmoi exoun os MSB to LLHH ara efarmozoume maska LLHH XXXX
	ori dekades_second, 0x30
	ori monades_minute, 0x30
	ori dekades_minute, 0x30

	ldi r24, 0x80					;epanafora kersora sthn arxh ths othomhs
	rcall lcd_command
	mov r24, dekades_minute				;emfanise ta MIN
	rcall lcd_data
	mov r24, monades_minute
	rcall lcd_data

	ldi r24, 0x87					;o kersoras sthn thesh 7 dld sthn DDRAM: 1000 0111
	rcall lcd_command
	mov r24, dekades_second				;emfanise ta SEC
	rcall lcd_data
	mov r24, monades_second
	rcall lcd_data

	andi monades_second, 0x0f			;ksanaepanefere otus kataxorites se morfh tetoia oste na ginontai oi prakseis dld 0000xxxx
	andi dekades_second, 0x0f
	andi monades_minute, 0x0f
	andi dekades_minute, 0x0f
	ret

main:
	ldi temp, high(RAMEND)
	out sph, temp
	ldi temp, low(RAMEND)
	out spl, temp
	ser temp
	out DDRD, temp                    
	clr temp
	out DDRB, temp                    ;h B os eisodos
	rcall lcd_init                    ;arxikopoihsh ths lcd
	rcall initialize_message          ;emfanisi tou axrikou minimatos
	
eternal_loop:
	sbic PINB, 7			  ;elegxos gia to an einai pathmeno to PB7
	rcall initialize_message	  ;an einai midenise to xronometro
	
	sbis PINB, 0			  ;elegxos gia to an einai pathmeno to PB0
	brcc eternal_loop		  ;an oxi tote mhn kaneis tipota

	rcall counter_increment		  ;auksisi kata ena sec
	rcall counter_to_lcd		  ;emfanise
	
	ldi r24, low(1000) 		  ;xronokathisterisi
	ldi r25, high(1000)
	rcall wait_msec

rjmp eternal_loop ;sunexes programma

write_2_nibbles: 
	push r24 
	in r25 ,PIND  
	andi r25 ,0x0f
	andi r24 ,0xf0 
	add r24 ,r25
	out PORTD ,r24 
	sbi PORTD ,PD3 
	cbi PORTD ,PD3 
	pop r24 
	swap r24 
	andi r24 ,0xf0 
	add r24 ,r25 
	out PORTD ,r24 
	sbi PORTD ,PD3  
	cbi PORTD ,PD3 
	ret

lcd_data: 
	sbi PORTD ,PD2  
	rcall write_2_nibbles 
	ldi r24 ,43
	ldi r25 ,0 
	rcall wait_usec 
	ret

lcd_command: 
	cbi PORTD ,PD2 
	rcall write_2_nibbles  
	ldi r24 ,39  
	ldi r25 ,0  
	rcall wait_usec 
	ret

lcd_init:     
	ldi r24 ,40 
	ldi r25 ,0
	rcall wait_msec 

	ldi r24 ,0x30 
	out PORTD ,r24 
	sbi PORTD ,PD3 
	cbi PORTD ,PD3 
	ldi r24 ,39 
	ldi r25 ,0 
	rcall wait_usec 
					
	ldi r24 ,0x30 
	out PORTD ,r24 
	sbi PORTD ,PD3 
	cbi PORTD ,PD3 
	ldi r24 ,39 
	ldi r25 ,0 
	rcall wait_usec 

	ldi r24 ,0x20
	out PORTD ,r24 
	sbi PORTD ,PD3 
	cbi PORTD ,PD3 
	ldi r24 ,39 
	ldi r25 ,0 
	rcall wait_usec 

	ldi r24 ,0x28 
	rcall lcd_command  

	ldi r24 ,0x0c  
	rcall lcd_command 

	ldi r24 ,0x01 
	rcall lcd_command 

	ldi r24 ,low(1530) 
	ldi r25 ,high(1530) 
	rcall wait_usec 

	ldi r24 ,0x06 
	rcall lcd_command  
	ret

wait_usec:    
	sbiw r24 ,1
	nop
	nop
	nop
	nop
	brne wait_usec
	ret

wait_msec: 
	push r24
	push r25
	ldi r24 , low(998)
	ldi r25 , high(998)
	rcall wait_usec
	pop r25
	pop r24
	sbiw r24 , 1
	brne wait_msec
	ret
Message:
.dw 0x3030, 0x4d20, 0x4e49, 0x303a, 0x2030, 0x4553, 0x0143	;apothikeush sto instraction memory
```

### Κώδικας σε C

To PB7 έχει μεγαλύτερη προτεραιοτητα απο το PB0 γι αυτό και το ελέγχουμε πριν απο το PB0 στη main.
Στο print_time για εμφανιση καθε φορα στην οθονη.

```c


#include <avr/io.h>
#define F_CPU 8000000UL
#include <util/delay.h>
#define _NOP() do { __asm__ __volatile__ ("nop"); } while (0)

char t[16] = {1,2,3,0xA,4,5,6,0xB,7,8,9,0xC,0xE,0,0xF,0xD};
char to_ascii[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

//x must be != 0
int msb(int x)
{
    int res = 0;
    while(x > 1){
        res++;
        x>>=1;
    }
    return res;
}

void pd3_pulse()
{
    char temp = PORTD;
    PORTD = temp | (1<<3);
    PORTD = temp;
}

void write_2_nibbles(char x)
{
    char lsb4 = PORTD & 0x0f;
    PORTD = lsb4 | (x & 0xf0);
    pd3_pulse();
    PORTD = lsb4 | (x << 4);
    pd3_pulse();
}

void lcd_data(char x)
{
    PORTD = PORTD | (1<<2);
    write_2_nibbles(x);
    _delay_us(43);
}

void lcd_command(char x)
{
    PORTD = PORTD & (~(1<<2));
    write_2_nibbles(x);
    _delay_us(39);
}

void lcd_init()   ;arxikopoiisi othonis
{
    DDRD = 0xff;

    _delay_ms(40);
    PORTD = 0x30;
    pd3_pulse();
    _delay_us(39);
    
    PORTD = 0x30;
    pd3_pulse();
    _delay_us(39);
    
    PORTD = 0x20;
    pd3_pulse();
    _delay_us(39);
    
    lcd_command(0x28);
    lcd_command(0x0c);
    lcd_command(0x01);
    
    _delay_us(1530);
    lcd_command(0x06);
    
}

volatile char min,sec;

void print_time()
{
	lcd_init();
	char monades = min % 10;         ;less significant psifio
	char dekades = (min/10) % 10;
	
	lcd_data(dekades + '0');
	lcd_data(monades + '0');
	lcd_data(' ');
	lcd_data('M');lcd_data('I');lcd_data('N');lcd_data(':');     ;emfanisi tou MIN: stin othoni
	
	monades = sec % 10;
	dekades = (sec/10) % 10;
	lcd_data(dekades + '0');
	lcd_data(monades + '0');
	lcd_data(' ');
	lcd_data('S');lcd_data('E');lcd_data('C');      ;emfanisi tou SEC stin othoni
}

void init_timer()     ;arxikopoiisi metriti
{
	min = 0;
	sec = 0;
	print_time();
}

int main()
{
    DDRB = 0x00;
	init_timer();

    while(1){
		unsigned char in = PINB;      ;eisodos h portB
		if((in & 0x80)){           ;an patithi to PB7 arxikopoiise metriti
			init_timer();
			continue;
		}
		if(in & 1){             ;o metritis douleuei oso to PB0 einai patimeno
			_delay_ms(1000);
			sec++;
			if(sec == 60){      ;otan ftasei sec telos midenise sec kai incr min
				min++;
				sec = 0;
			}
			if(min == 60)        ;otan ftasei min telos midenise
				min = sec = 0;
			print_time();
		}
	}
}
```
