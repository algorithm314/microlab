#include <avr/io.h>
#define F_CPU 8000000UL
#include <util/delay.h>
#define _NOP() do { __asm__ __volatile__ ("nop"); } while (0)

void init_keyboard()
{
    DDRC = 0xf0;
}

//x must be != 0
int msb(int x)
{
    int res = 0;
    while(x > 1){
        res++;
        x>>=1;
    }
    return res;
}

//only one key pressed at a time
//if nothing pressed: return -1
int scan_keypad()
{
    int key;
    for(int i = 4;i < 8;i++){
        PORTC = 1<<i;
        _NOP();
		_NOP();
        key = PINC & 0xf;
        if(key != 0)
            return 4*(i-4)+msb(key);
    }
    return -1;
}
int prev = -1;

int read_spark(){
	int t1,t2;
	do{
		t1 = scan_keypad();
		_delay_ms (16);
		t2 = scan_keypad();
	}while(t1 != t2);
	return t2;
}
int scan_rising_edge()
{
    do{
        int tmp;
        while((tmp = read_spark()) == prev)
            ;
        prev = tmp;
    } while(prev == -1);
	return prev;
}

int main()
{
    DDRB = 0xff;
    init_keyboard();
    while(1){
        int k1 = scan_rising_edge();
        int k2 = scan_rising_edge();
        if(k1 == 13 && k2 == 8){
            PORTB = 0xff;
            _delay_ms (4000);
			PORTB = 0;
        }else{
            for(int i = 0;i < 8;i++){
                PORTB = 0xff;
                _delay_ms (250);
                PORTB = 0;
                 _delay_ms (250);
            }
                
        }
        
    }
}
