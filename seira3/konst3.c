#include <avr/io.h>
#define F_CPU 8000000UL
#include <util/delay.h>
#define _NOP() do { __asm__ __volatile__ ("nop"); } while (0)

char t[16] = {1,2,3,0xA,4,5,6,0xB,7,8,9,0xC,0xE,0,0xF,0xD};
char to_ascii[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

//x must be != 0
int msb(int x)
{
    int res = 0;
    while(x > 1){
        res++;
        x>>=1;
    }
    return res;
}

void pd3_pulse()
{
    char temp = PORTD;
    PORTD = temp | (1<<3);
    PORTD = temp;
}

void write_2_nibbles(char x)
{
    char lsb4 = PORTD & 0x0f;
    PORTD = lsb4 | (x & 0xf0);
    pd3_pulse();
    PORTD = lsb4 | (x << 4);
    pd3_pulse();
}

void lcd_data(char x)
{
    PORTD = PORTD | (1<<2);
    write_2_nibbles(x);
    _delay_us(43);
}

void lcd_command(char x)
{
    PORTD = PORTD & (~(1<<2));
    write_2_nibbles(x);
    _delay_us(39);
}

void lcd_init()
{
    DDRD = 0xff;

    _delay_ms(40);
    PORTD = 0x30;
    pd3_pulse();
    _delay_us(39);
    
    PORTD = 0x30;
    pd3_pulse();
    _delay_us(39);
    
    PORTD = 0x20;
    pd3_pulse();
    _delay_us(39);
    
    lcd_command(0x28);
    lcd_command(0x0c);
    lcd_command(0x01);
    
    _delay_us(1530);
    lcd_command(0x06);
    
}

volatile char min,sec;

void print_time()
{
	lcd_init();
	char monades = min % 10;
	char dekades = (min/10) % 10;
	
	lcd_data(dekades + '0');
	lcd_data(monades + '0');
	lcd_data(' ');
	lcd_data('M');lcd_data('I');lcd_data('N');lcd_data(':');
	
	monades = sec % 10;
	dekades = (sec/10) % 10;
	lcd_data(dekades + '0');
	lcd_data(monades + '0');
	lcd_data(' ');
	lcd_data('S');lcd_data('E');lcd_data('C');
}

void init_timer()
{
	min = 0;
	sec = 0;
	print_time();
}

int main()
{
    DDRB = 0x00;
	init_timer();

    while(1){
		unsigned char in = PINB;
		if((in & 0x80)){
			init_timer();
			continue;
		}
		if(in & 1){
			_delay_ms(1000);
			sec++;
			if(sec == 60){
				min++;
				sec = 0;
			}
			if(min == 60)
				min = sec = 0;
			print_time();
		}
	}
}
