; MSB ON left -> right
; LSB OFF nothing working

MVI C,80H ; 127 mask
MVI A,80H

L:
	CMA
	OUT 30H
	CMA
	MOV B,A
	CALL DELAY_05
	IN 20H
	MOV D,A
	
	ANI 80H
	CMP C
	JZ LEFT_TO_RIGHT

RIGHT_TO_LEFT:
	MOV A,D
	ANI 01H
	JZ STOP
	MOV A,B
	RAL
	JNC L
	
	;CHANGE DIRECTION
	MVI A,80H
	XRA C
	MOV C,A
	
	MVI A,40H ;set bit
	
NOTHING_SPECIAL:
	JMP L

LEFT_TO_RIGHT:
	MOV A,D
	ANI 01H
	JZ STOP
	MOV A,B
	RAR
	JNC L
	
	;CHANGE DIRECTION
	MVI A,80H
	XRA C
	MOV C,A

	
	MVI A,02H ;set bit
	JMP L

STOP:
	MOV A,B
	JMP L

DELAY_05:
	PUSH B
	MVI B,01H
	MVI C,F4H
	CALL DELB
	POP B
	RET

END
